﻿using System.Data.Entity;
namespace Domain
{
    public class Context: DbContext
    {
        public DbSet<PlantComponent> PlantComponents { get; set; }
        public DbSet<Variable> Variables { get; set; }
        public DbSet<DeviceType> DeviceTypes { get; set; }
        public DbSet<Device> Devices { get; set; }
        public DbSet<Installation> Installations { get; set; }
        public DbSet<IndustrialPlant> IndustrialPlants { get; set; }
    }
}
