﻿using Domain;
using Persistence;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;

namespace DataBasePersistence
{
    public class DataBaseQueries : DomainPersistence
    {
        public bool AddDevice(Device device)
        {

            using (var scadaSystemContext = new Context())
            {
                foreach (var variable in device.Variables)
                {
                    scadaSystemContext.Variables.Attach(variable);
                }
                if (device.Parent != null)
                {
                    scadaSystemContext.PlantComponents.Attach(device.Parent);
                }
                scadaSystemContext.DeviceTypes.Attach(device.Type);
                scadaSystemContext.Devices.Add(device);
                scadaSystemContext.SaveChanges();
                return true;
            }
        }

        public bool AddDeviceType(DeviceType deviceType)
        {
            using (var scadaSystemContext = new Context())
            {
                scadaSystemContext.DeviceTypes.Add(deviceType);
                scadaSystemContext.SaveChanges();
                return true;
            }
        }

        public bool AddInstallation(Installation installation)
        {
            using (var scadaSystemContext = new Context())
            {
                foreach (var component in installation.ChildrenComponents)
                {
                    scadaSystemContext.PlantComponents.Attach(component);
                }
                foreach (var variable in installation.Variables)
                {
                    scadaSystemContext.Variables.Attach(variable);
                }
                if (installation.Parent != null)
                {
                    scadaSystemContext.PlantComponents.Attach(installation.Parent);
                }

                scadaSystemContext.Installations.Add(installation);
                scadaSystemContext.SaveChanges();
                return true;

            }
        }

        public bool AddIndustrialPlant(IndustrialPlant industrialPlant)
        {
            using(var scadaSystemContext = new Context())
            {
                foreach (var component in industrialPlant.ChildrenComponents)
                {
                    scadaSystemContext.PlantComponents.Attach(component);
                }
                if (industrialPlant.Parent != null)
                {
                    scadaSystemContext.PlantComponents.Attach(industrialPlant.Parent);
                }
                scadaSystemContext.IndustrialPlants.Add(industrialPlant);
                scadaSystemContext.SaveChanges();
                return true;
            }
        }

        public bool AddPlantComponent(PlantComponent plantComponent)
        {
            using (var scadaSystemContext = new Context())
            {
                foreach (var variable in plantComponent.Variables)
                {
                    scadaSystemContext.Variables.Attach(variable);
                }
                if (plantComponent.Parent != null)
                {
                    scadaSystemContext.PlantComponents.Attach(plantComponent.Parent);
                }
                scadaSystemContext.PlantComponents.Add(plantComponent);
                scadaSystemContext.SaveChanges();
                return true;
            }
        }

        public bool DeleteDevice(Device device)
        {
            using (var scadaSystemContext = new Context())
            {
                Device deviceToDelete;
                var deviceFound = from devices in scadaSystemContext.Devices
                                  where devices.Identifier == device.Identifier
                                  select devices;
                deviceToDelete = deviceFound.SingleOrDefault();
                scadaSystemContext.Devices.Remove(deviceToDelete);
                scadaSystemContext.SaveChanges();
                return true;
            }
        }

        public bool DeleteInstallation(Installation installation)
        {
            using (var scadaSystemContext = new Context())
            {
                Installation installationToDelete;
                var installationFound = from installations in scadaSystemContext.Installations
                                        where installations.Identifier == installation.Identifier
                                        select installations;
                installationToDelete = installationFound.SingleOrDefault();
                scadaSystemContext.Installations.Remove(installationToDelete);
                scadaSystemContext.SaveChanges();
                return true;
            }
        }

        public bool DeleteIndustrialPlant(IndustrialPlant industrialPlant)
        {
            using(var scadaSystemContext = new Context())
            {
                IndustrialPlant industrialPlantToDelete;
                var industrialPlantFound = from industrialPlants in scadaSystemContext.IndustrialPlants
                                           where industrialPlants.Identifier == industrialPlant.Identifier
                                           select industrialPlants;
                industrialPlantToDelete = industrialPlantFound.SingleOrDefault();
                scadaSystemContext.IndustrialPlants.Remove(industrialPlantToDelete);
                scadaSystemContext.SaveChanges();
                return true;
            }
        }

        public bool DeletePlantComponent(PlantComponent plantComponent)
        {
            using (var scadaSystemContext = new Context())
            {
                PlantComponent plantComponentToDelete;
                var plantComponentFound = from plantComponents in scadaSystemContext.PlantComponents
                                          where plantComponents.Identifier == plantComponent.Identifier
                                          select plantComponents;
                plantComponentToDelete = plantComponentFound.SingleOrDefault();
                scadaSystemContext.PlantComponents.Remove(plantComponentToDelete);
                scadaSystemContext.SaveChanges();
                return true;
            }
        }

        public List<Device> ListDevices()
        {
            List<Device> allDevices = new List<Device>();
            using (var scadaSystemContext = new Context())
            {
                var getAllDevices = from devices in scadaSystemContext.Devices.Include("Variables").Include("Type").Include("Parent")
                                    select devices;

                foreach (var deviceInDB in getAllDevices)
                {
                    allDevices.Add(deviceInDB);
                }
                return allDevices;
            }
        }

        public List<DeviceType> ListDeviceTypes()
        {
            List<DeviceType> allDeviceTypes = new List<DeviceType>();
            using (var scadaSystemContext = new Context())
            {
                var getAllDevicetypes = from deviceTypes in scadaSystemContext.DeviceTypes
                                        select deviceTypes;

                foreach (var deviceTypeInDB in getAllDevicetypes)
                {
                    allDeviceTypes.Add(deviceTypeInDB);
                }
                return allDeviceTypes;
            }
        }

        public List<PlantComponent> ListIndustrialPlantChildrenComponents(IndustrialPlant industrialPlant)
        {
            try
            {
                using(var scadaSystemContext = new Context())
                {
                    scadaSystemContext.IndustrialPlants.Attach(industrialPlant);
                    return industrialPlant.ChildrenComponents;
                }
            }
            catch(SqlException sqlException)
            {
                throw;
            }
        }

        public List<PlantComponent> ListInstallationChildrenComponents(Installation installation)
        {
            try
            {
                using (var scadaSystemContext = new Context())
                {
                    scadaSystemContext.Installations.Attach(installation);
                    return installation.ChildrenComponents;
                }
            }
            catch (SqlException sqlException)
            {
                throw;
            }
        }
    

        public List<Installation> ListInstallations()
        {
            List<Installation> allInstallations = new List<Installation>();
            using (var scadaSystemContext = new Context())
            {
                var getAllInstallations = from installations in scadaSystemContext.Installations.Include("ChildrenComponents").Include("Variables").Include("Parent").Include("ChildrenComponents.Variables")
                                          select installations;

                foreach (var installationInDB in getAllInstallations)
                {
                    allInstallations.Add(installationInDB);
                }
                return allInstallations;
            }
        }

        public List<IndustrialPlant> ListIndustrialPlants()
        {
            List<IndustrialPlant> allIndustrialPlants = new List<IndustrialPlant>();
            using(var scadaSystemContext = new Context())
            {
                var getAllIndustrialPlants = from industrialPlants in scadaSystemContext.IndustrialPlants.Include("ChildrenComponents").Include("Parent").Include("Variables")
                                          select industrialPlants;

                foreach (var industrialPlantInDB in getAllIndustrialPlants)
                {
                    allIndustrialPlants.Add(industrialPlantInDB);
                }
                return allIndustrialPlants;
            }
        }
        public List<PlantComponent> ListPlantComponents()
        {
            List<PlantComponent> allPlantComponents = new List<PlantComponent>();
            using (var scadaSystemContext = new Context())
            {
                var getAllPlantComponents = from plantComponents in scadaSystemContext.PlantComponents.Include("Variables")
                                            select plantComponents;

                foreach (var plantComponentInDB in getAllPlantComponents)
                {
                    allPlantComponents.Add(plantComponentInDB);
                }
                return allPlantComponents;
            }
        }

        public bool ModifyDevice(Device device)
        {
            using (var scadaSystemContext = new Context())
            {
                try
                {
                    var deviceInDB = scadaSystemContext.Devices.Include("Parent").Include("Variables").Include("Type").Single(d => d.Identifier == device.Identifier);

                    deviceInDB.Name = device.Name;
                    deviceInDB.OutOfRangeVariablesNumber = device.OutOfRangeVariablesNumber;
                    deviceInDB.OutOfWarningRangeVariablesNumber = device.OutOfWarningRangeVariablesNumber;
                    if (device.Parent != null)
                    {
                        deviceInDB.Parent = scadaSystemContext.PlantComponents.Include("Parent").Single(p => p.Identifier == device.Parent.Identifier);
                    }
                    var deviceTypeToAdd = scadaSystemContext.DeviceTypes.Single(dt => dt.Id == device.Type.Id);
                    deviceInDB.Type = deviceTypeToAdd;
                    foreach (var variableInDevice in device.Variables)
                    {
                        if (deviceInDB.Variables.Contains(variableInDevice))
                        {
                            var variableToModify = scadaSystemContext.Variables.Single(v => v.Identifier == variableInDevice.Identifier);
                            variableToModify.CopyDataFrom(variableInDevice);
                        }
                        else
                        {
                            deviceInDB.Variables.Add(variableInDevice);
                        }
                    }

                    List<Variable> variablesToDelete = new List<Variable>();
                    foreach (var variableInDeviceInDB in deviceInDB.Variables)
                    {
                        if (!device.Variables.Contains(variableInDeviceInDB))
                        {
                            variablesToDelete.Add(variableInDeviceInDB);
                        }
                    }

                    foreach (var variableToDeleteFromDeviceInDB in variablesToDelete)
                    {
                        deviceInDB.Variables.Remove(variableToDeleteFromDeviceInDB);
                        scadaSystemContext.Variables.Remove(variableToDeleteFromDeviceInDB);
                    }
                    PlantComponent firstLevelParent = deviceInDB.GetFirstLevelParent();
                    var parentInDB = scadaSystemContext.PlantComponents.Include("Variables").Include("Parent").Single(i => i.Identifier == firstLevelParent.Identifier);
                    parentInDB.RefreshOutOfRangeVariableNumber();
                    scadaSystemContext.SaveChanges();
                    return true;
                }
                catch
                {
                    throw;
                }
            }
        }

        public bool ModifyDeviceType(DeviceType deviceType)
        {
            using (var scadaSystemContext = new Context())
            {
                DeviceType deviceTypeToModify;
                var deviceTypeFound = from deviceTypes in scadaSystemContext.DeviceTypes
                                      where deviceTypes.Id == deviceType.Id
                                      select deviceTypes;

                deviceTypeToModify = deviceTypeFound.SingleOrDefault();
                deviceTypeToModify.CopyDataFrom(deviceType);
                scadaSystemContext.SaveChanges();
                return true;
            }
        }

        public bool ModifyInstallation(Installation installation)
        {
            using (var scadaSystemContext = new Context())
            {
                try
                {
                    var installationInDB = scadaSystemContext.Installations.Include("ChildrenComponents").Include("Variables").Include("Parent").Single(i => i.Identifier == installation.Identifier);

                    installationInDB.Name = installation.Name;
                    installationInDB.OutOfRangeVariablesNumber = installation.OutOfRangeVariablesNumber;
                    installationInDB.OutOfWarningRangeVariablesNumber = installation.OutOfWarningRangeVariablesNumber;
                    if(installation.Parent != null)
                    {
                        installationInDB.Parent = scadaSystemContext.PlantComponents.Include("Parent").Single(p => p.Identifier == installation.Parent.Identifier);
                    }
                    
                    List<PlantComponent> componentsToDelete = new List<PlantComponent>();

                    foreach (var componentAssignedInDB in installationInDB.ChildrenComponents)
                    {
                        if (!installation.ChildrenComponents.Contains(componentAssignedInDB))
                        {
                            componentsToDelete.Add(componentAssignedInDB);
                        }
                    }

                    foreach(var componentsToDeleteFromInstallation in componentsToDelete)
                    {
                        componentsToDeleteFromInstallation.Parent = null;
                        installationInDB.ChildrenComponents.Remove(componentsToDeleteFromInstallation);
                    }

                    foreach (var recentlyAddedComponent in installation.ChildrenComponents)
                    {
                        if (!installationInDB.ChildrenComponents.Contains(recentlyAddedComponent))
                        {
                            var recentlyAddedComponentInDB = scadaSystemContext.PlantComponents.Include("Parent").Single(c => c.Identifier == recentlyAddedComponent.Identifier);
                            recentlyAddedComponentInDB.Parent = installationInDB;
                            installationInDB.ChildrenComponents.Add(recentlyAddedComponentInDB);
                        }
                    }

                    foreach (var variableInInstallation in installation.Variables)
                    {
                        if (installationInDB.Variables.Contains(variableInInstallation))
                        {
                            var variableToModify = scadaSystemContext.Variables.Single(v => v.Identifier == variableInInstallation.Identifier);
                            variableToModify.CopyDataFrom(variableInInstallation);
                        }
                        else
                        {
                            installationInDB.Variables.Add(variableInInstallation);
                        }
                    }

                    List<Variable> variablesToDelete = new List<Variable>();
                    foreach (var variableInInstallationInDB in installationInDB.Variables)
                    {
                        if (!installation.Variables.Contains(variableInInstallationInDB))
                        {
                            variablesToDelete.Add(variableInInstallationInDB);
                        }
                    }

                    foreach (var variableToDeleteFromInstallationInDB in variablesToDelete)
                    {
                        installationInDB.Variables.Remove(variableToDeleteFromInstallationInDB);
                        scadaSystemContext.Variables.Remove(variableToDeleteFromInstallationInDB);
                    }
                    var firstLevelParent = installationInDB.GetFirstLevelParent();
                    var parentInDB = scadaSystemContext.PlantComponents.Include("Variables").Include("Parent").Single(i => i.Identifier == firstLevelParent.Identifier);
                    parentInDB.RefreshOutOfRangeVariableNumber();
                    scadaSystemContext.SaveChanges();
                    return true;
                }
                catch
                {
                    throw;
                }
            }
        }

        public bool ModifyIndustrialPlant(IndustrialPlant industrialPlant)
        {
            using (var scadaSystemContext = new Context())
            {
                try
                {
                    var industrialPlantInDB = scadaSystemContext.IndustrialPlants.Include("ChildrenComponents").Include("Variables").Include("Parent").Single(i => i.Identifier == industrialPlant.Identifier);

                    industrialPlantInDB.Name = industrialPlant.Name;
                    industrialPlantInDB.OutOfRangeVariablesNumber = industrialPlant.OutOfRangeVariablesNumber;
                    industrialPlantInDB.OutOfWarningRangeVariablesNumber = industrialPlant.OutOfWarningRangeVariablesNumber;
                    industrialPlantInDB.Parent = industrialPlant.Parent;
                    List<PlantComponent> componentsToDelete = new List<PlantComponent>();

                    foreach (var componentAssignedInDB in industrialPlantInDB.ChildrenComponents)
                    {
                        if (!industrialPlant.ChildrenComponents.Contains(componentAssignedInDB))
                        {
                            componentsToDelete.Add(componentAssignedInDB);
                        }
                    }

                    foreach (var componentsToDeleteFromIndustrialPlant in componentsToDelete)
                    {
                        componentsToDeleteFromIndustrialPlant.Parent = null;
                        industrialPlantInDB.ChildrenComponents.Remove(componentsToDeleteFromIndustrialPlant);
                    }

                    foreach (var recentlyAddedComponent in industrialPlant.ChildrenComponents)
                    {
                        if (!industrialPlantInDB.ChildrenComponents.Contains(recentlyAddedComponent))
                        {
                            var recentlyAddedComponentInDB = scadaSystemContext.PlantComponents.Include("Parent").Single(c => c.Identifier == recentlyAddedComponent.Identifier);
                            recentlyAddedComponentInDB.Parent = industrialPlantInDB;
                            industrialPlantInDB.ChildrenComponents.Add(recentlyAddedComponentInDB);
                        }
                    }
                    PlantComponent firstLevelParent = industrialPlantInDB.GetFirstLevelParent();
                    var parentInDB = scadaSystemContext.PlantComponents.Include("Variables").Include("Parent").Single(i => i.Identifier == firstLevelParent.Identifier);
                    parentInDB.RefreshOutOfRangeVariableNumber();
                    scadaSystemContext.SaveChanges();
                    return true;
                }
                catch
                {
                    throw;
                }
            }
        }

        public bool ModifyPlantComponent(PlantComponent plantComponent)
        {
            using (var scadaSystemContext = new Context())
            {
                if (plantComponent.IsDevice())
                {
                    return ModifyDevice(plantComponent as Device);
                }
                if (plantComponent.IsInstallation())
                {
                    return ModifyInstallation(plantComponent as Installation);
                }
                if (plantComponent.IsProductionPlant())
                {
                    return ModifyIndustrialPlant(plantComponent as IndustrialPlant);
                }
                return false;
            }
        }

        public PlantComponent GetFirstLevelParent(PlantComponent plantComponent)
        {
            using (var scadaSystemContext = new Context())
            {
                if (plantComponent.Parent == null)
                {
                    return plantComponent;
                }
                else
                {
                    var plantComponentParent = scadaSystemContext.PlantComponents.Include("Parent").Include("Variables").
                        Single(p => p.Identifier == plantComponent.Parent.Identifier);
                    return GetFirstLevelParent(plantComponentParent);
                }
            }             
        }

        public List<Installation> ListIndustrialPlantFirstLevelInstallations(IndustrialPlant industrialPlant)
        {
            using (var scadaSystemContext = new Context())
            {
                var industrialPlantFirstLevelInstallations = from i in scadaSystemContext.Installations.
                                                             Include("Parent").Include("Variables").Include("ChildrenComponents")
                                                             where i.Parent.Identifier == industrialPlant.Identifier
                                                             select i;

                return industrialPlantFirstLevelInstallations.ToList<Installation>();
            }
        }
    }
}
