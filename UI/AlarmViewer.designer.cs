﻿namespace UI
{
    partial class AlarmViewer
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.picBox_AlarmIcon = new System.Windows.Forms.PictureBox();
            this.lbl_ComponentData = new System.Windows.Forms.Label();
            this.pnl_AlarmViewer = new System.Windows.Forms.Panel();
            this.label_WarningsNumber = new System.Windows.Forms.Label();
            this.label_AlarmsNumber = new System.Windows.Forms.Label();
            this.label2_WarningsNumberText = new System.Windows.Forms.Label();
            this.label_AlarmsNumberText = new System.Windows.Forms.Label();
            this.Button_DrillDown = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.picBox_AlarmIcon)).BeginInit();
            this.pnl_AlarmViewer.SuspendLayout();
            this.SuspendLayout();
            // 
            // picBox_AlarmIcon
            // 
            this.picBox_AlarmIcon.Location = new System.Drawing.Point(11, 3);
            this.picBox_AlarmIcon.Name = "picBox_AlarmIcon";
            this.picBox_AlarmIcon.Size = new System.Drawing.Size(50, 50);
            this.picBox_AlarmIcon.TabIndex = 0;
            this.picBox_AlarmIcon.TabStop = false;
            // 
            // lbl_ComponentData
            // 
            this.lbl_ComponentData.BackColor = System.Drawing.SystemColors.Control;
            this.lbl_ComponentData.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_ComponentData.ForeColor = System.Drawing.Color.Black;
            this.lbl_ComponentData.Location = new System.Drawing.Point(5, 56);
            this.lbl_ComponentData.Name = "lbl_ComponentData";
            this.lbl_ComponentData.Size = new System.Drawing.Size(156, 28);
            this.lbl_ComponentData.TabIndex = 1;
            this.lbl_ComponentData.Text = "label";
            this.lbl_ComponentData.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnl_AlarmViewer
            // 
            this.pnl_AlarmViewer.Controls.Add(this.Button_DrillDown);
            this.pnl_AlarmViewer.Controls.Add(this.label_WarningsNumber);
            this.pnl_AlarmViewer.Controls.Add(this.label_AlarmsNumber);
            this.pnl_AlarmViewer.Controls.Add(this.label2_WarningsNumberText);
            this.pnl_AlarmViewer.Controls.Add(this.label_AlarmsNumberText);
            this.pnl_AlarmViewer.Controls.Add(this.lbl_ComponentData);
            this.pnl_AlarmViewer.Controls.Add(this.picBox_AlarmIcon);
            this.pnl_AlarmViewer.Location = new System.Drawing.Point(-2, -2);
            this.pnl_AlarmViewer.Name = "pnl_AlarmViewer";
            this.pnl_AlarmViewer.Size = new System.Drawing.Size(159, 146);
            this.pnl_AlarmViewer.TabIndex = 2;
            // 
            // label_WarningsNumber
            // 
            this.label_WarningsNumber.AutoSize = true;
            this.label_WarningsNumber.Location = new System.Drawing.Point(108, 123);
            this.label_WarningsNumber.Name = "label_WarningsNumber";
            this.label_WarningsNumber.Size = new System.Drawing.Size(0, 13);
            this.label_WarningsNumber.TabIndex = 5;
            // 
            // label_AlarmsNumber
            // 
            this.label_AlarmsNumber.AutoSize = true;
            this.label_AlarmsNumber.Location = new System.Drawing.Point(108, 98);
            this.label_AlarmsNumber.Name = "label_AlarmsNumber";
            this.label_AlarmsNumber.Size = new System.Drawing.Size(0, 13);
            this.label_AlarmsNumber.TabIndex = 4;
            // 
            // label2_WarningsNumberText
            // 
            this.label2_WarningsNumberText.AutoSize = true;
            this.label2_WarningsNumberText.Location = new System.Drawing.Point(8, 123);
            this.label2_WarningsNumberText.Name = "label2_WarningsNumberText";
            this.label2_WarningsNumberText.Size = new System.Drawing.Size(93, 13);
            this.label2_WarningsNumberText.TabIndex = 3;
            this.label2_WarningsNumberText.Text = "Warnings number:";
            // 
            // label_AlarmsNumberText
            // 
            this.label_AlarmsNumberText.AutoSize = true;
            this.label_AlarmsNumberText.Location = new System.Drawing.Point(8, 98);
            this.label_AlarmsNumberText.Name = "label_AlarmsNumberText";
            this.label_AlarmsNumberText.Size = new System.Drawing.Size(79, 13);
            this.label_AlarmsNumberText.TabIndex = 2;
            this.label_AlarmsNumberText.Text = "Alarms number:";
            // 
            // Button_DrillDown
            // 
            this.Button_DrillDown.Location = new System.Drawing.Point(65, 17);
            this.Button_DrillDown.Name = "Button_DrillDown";
            this.Button_DrillDown.Size = new System.Drawing.Size(88, 23);
            this.Button_DrillDown.TabIndex = 6;
            this.Button_DrillDown.Text = "Ver detalle";
            this.Button_DrillDown.UseVisualStyleBackColor = true;
            // 
            // AlarmViewer
            // 
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Controls.Add(this.pnl_AlarmViewer);
            this.Name = "AlarmViewer";
            this.Size = new System.Drawing.Size(156, 142);
            ((System.ComponentModel.ISupportInitialize)(this.picBox_AlarmIcon)).EndInit();
            this.pnl_AlarmViewer.ResumeLayout(false);
            this.pnl_AlarmViewer.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox picBox_AlarmIcon;
        private System.Windows.Forms.Label lbl_ComponentData;
        private System.Windows.Forms.Panel pnl_AlarmViewer;
        private System.Windows.Forms.Label label_WarningsNumber;
        private System.Windows.Forms.Label label_AlarmsNumber;
        private System.Windows.Forms.Label label2_WarningsNumberText;
        private System.Windows.Forms.Label label_AlarmsNumberText;
        private System.Windows.Forms.Button Button_DrillDown;
    }
}
