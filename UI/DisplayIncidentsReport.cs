﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Domain;

namespace UI
{
    public partial class DisplayIncidentsReport : Form
    {
        private Tuple<DateTime, DateTime> dateRangeFilters;
        private PlantComponent plantComponentFilter;
        private int severity;
        private bool executeRecursiveSearch;

        public DisplayIncidentsReport(Tuple<DateTime, DateTime> dateRangeFilters, int severity,  PlantComponent plantComponentFilter, bool executeRecursiveSearch)
        {
            this.dateRangeFilters = dateRangeFilters;
            this.plantComponentFilter = plantComponentFilter;
            this.severity = severity;
            this.executeRecursiveSearch = executeRecursiveSearch;
            InitializeComponent();
            LoadFilteredIncidentsInListBox();
        }


        private void LoadFilteredIncidentsInListBox()
        {
            List<Incident> filteredIncidents = ProductionPlant.GetInstance().GetIncidentsFilteredBy(dateRangeFilters, severity, plantComponentFilter, executeRecursiveSearch);
            filteredIncidents.Sort();
            listBox_FilteredIncidents.Items.AddRange(filteredIncidents.ToArray());
        }
    }
}
