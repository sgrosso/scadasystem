﻿using Domain;
using System;
using System.Windows.Forms;
using IncidentReporting;

namespace UI
{
    public partial class SetReportingMethod : Form
    {
        public SetReportingMethod()
        {
            InitializeComponent();
            SelectRadioButtonBasedOnActualReportingMethod();
        }

        private void Button_Ok_Click(object sender, EventArgs e)
        {
            if (radio_DBMethod.Checked)
            {
                Properties.Settings.Default.IncidentReportingMethod = "DB";
                Properties.Settings.Default.Save();
                ProductionPlant.GetInstance().SetIncidentLoadingMethod(new IncidentDatabaseLoader());
                ProductionPlant.GetInstance().SetIncidentReportingMethod(new IncidentDatabaseReporter());
            }
            else
            {
                Properties.Settings.Default.IncidentReportingMethod = "TXT";
                Properties.Settings.Default.Save();
                ProductionPlant.GetInstance().SetIncidentLoadingMethod(new IncidentTextFileLoader());
                ProductionPlant.GetInstance().SetIncidentReportingMethod(new IncidentTextFileReporter());
            }
            this.Dispose();
        }

        private void SelectRadioButtonBasedOnActualReportingMethod()
        {
            String actualReportingMethod = Properties.Settings.Default.IncidentReportingMethod;
            if(actualReportingMethod == "TXT")
            {
                Radio_TxtMethod.Checked = true;
            }
            else
            {
                if (actualReportingMethod == "DB")
                {
                    radio_DBMethod.Checked = true;
                }
            }

        }

        private void Button_Cancel_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
