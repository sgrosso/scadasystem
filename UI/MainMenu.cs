﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI
{
    public partial class MainMenu : Form
    {
        public MainMenu()
        {
            InitializeComponent();
        }

        private void MaintenanceDevicesButton_Click(object sender, EventArgs e)
        {
            int MAINTENANCE_TYPE = 3;
            Maintenance windowMaintenance = new Maintenance(MAINTENANCE_TYPE);
            windowMaintenance.Show();
        }

        private void MaintenanceInstallationButton_Click(object sender, EventArgs e)
        {
            int MAINTENANCE_TYPE = 2;
            Maintenance windowMaintenance = new Maintenance(MAINTENANCE_TYPE);
            windowMaintenance.Show();
        }

        private void Button_ControlPanel_Click(object sender, EventArgs e)
        {
            ControlPanelIndustrialPlantSelection industrialPlantSelection = new ControlPanelIndustrialPlantSelection();
            industrialPlantSelection.Show();
        }

        private void maintenanceIndustrialPlantButton_Click(object sender, EventArgs e)
        {
            int MAINTENANCE_TYPE = 1;
            Maintenance windowMaintenance = new Maintenance(MAINTENANCE_TYPE);
            windowMaintenance.Show();
        }

        private void Button_SetReportMethodConfiguration_Click(object sender, EventArgs e)
        {
            SetReportingMethod reportingMethodConfigurationWindow = new SetReportingMethod();
            reportingMethodConfigurationWindow.Show();
        }
    }
}
