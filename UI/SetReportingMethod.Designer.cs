﻿namespace UI
{
    partial class SetReportingMethod
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Group_ReportingMethods = new System.Windows.Forms.GroupBox();
            this.radio_DBMethod = new System.Windows.Forms.RadioButton();
            this.Radio_TxtMethod = new System.Windows.Forms.RadioButton();
            this.Button_Ok = new System.Windows.Forms.Button();
            this.Button_Cancel = new System.Windows.Forms.Button();
            this.Group_ReportingMethods.SuspendLayout();
            this.SuspendLayout();
            // 
            // Group_ReportingMethods
            // 
            this.Group_ReportingMethods.Controls.Add(this.radio_DBMethod);
            this.Group_ReportingMethods.Controls.Add(this.Radio_TxtMethod);
            this.Group_ReportingMethods.Location = new System.Drawing.Point(12, 12);
            this.Group_ReportingMethods.Name = "Group_ReportingMethods";
            this.Group_ReportingMethods.Size = new System.Drawing.Size(260, 110);
            this.Group_ReportingMethods.TabIndex = 2;
            this.Group_ReportingMethods.TabStop = false;
            this.Group_ReportingMethods.Text = "Reporting Methods";
            // 
            // radio_DBMethod
            // 
            this.radio_DBMethod.AutoSize = true;
            this.radio_DBMethod.Location = new System.Drawing.Point(59, 66);
            this.radio_DBMethod.Name = "radio_DBMethod";
            this.radio_DBMethod.Size = new System.Drawing.Size(110, 17);
            this.radio_DBMethod.TabIndex = 1;
            this.radio_DBMethod.TabStop = true;
            this.radio_DBMethod.Text = "Database Method";
            this.radio_DBMethod.UseVisualStyleBackColor = true;
            // 
            // Radio_TxtMethod
            // 
            this.Radio_TxtMethod.AutoSize = true;
            this.Radio_TxtMethod.Location = new System.Drawing.Point(59, 33);
            this.Radio_TxtMethod.Name = "Radio_TxtMethod";
            this.Radio_TxtMethod.Size = new System.Drawing.Size(101, 17);
            this.Radio_TxtMethod.TabIndex = 0;
            this.Radio_TxtMethod.TabStop = true;
            this.Radio_TxtMethod.Text = "Text file Method";
            this.Radio_TxtMethod.UseVisualStyleBackColor = true;
            // 
            // Button_Ok
            // 
            this.Button_Ok.Location = new System.Drawing.Point(51, 140);
            this.Button_Ok.Name = "Button_Ok";
            this.Button_Ok.Size = new System.Drawing.Size(75, 23);
            this.Button_Ok.TabIndex = 3;
            this.Button_Ok.Text = "OK";
            this.Button_Ok.UseVisualStyleBackColor = true;
            this.Button_Ok.Click += new System.EventHandler(this.Button_Ok_Click);
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.Location = new System.Drawing.Point(158, 140);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.TabIndex = 4;
            this.Button_Cancel.Text = "Cancel";
            this.Button_Cancel.UseVisualStyleBackColor = true;
            this.Button_Cancel.Click += new System.EventHandler(this.Button_Cancel_Click);
            // 
            // SetReportingMethod
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 175);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_Ok);
            this.Controls.Add(this.Group_ReportingMethods);
            this.Name = "SetReportingMethod";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configure Reporting Method";
            this.Group_ReportingMethods.ResumeLayout(false);
            this.Group_ReportingMethods.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox Group_ReportingMethods;
        private System.Windows.Forms.RadioButton radio_DBMethod;
        private System.Windows.Forms.RadioButton Radio_TxtMethod;
        private System.Windows.Forms.Button Button_Ok;
        private System.Windows.Forms.Button Button_Cancel;
    }
}