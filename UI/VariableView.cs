﻿using Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI
{
    public partial class VariableView : Form
    {
        PlantComponent plantComponentParent;
        public VariableView(PlantComponent plantComponent)
        {
            InitializeComponent();
            plantComponentParent = plantComponent;
            LoadData();
        }

        private void LoadData()
        {
            listBoxVariables.Items.Clear();
            listBoxVariables.Items.AddRange(plantComponentParent.Variables.ToArray());
            textBoxValue.Enabled = false;
            buttonSave.Enabled = false;
            labelRange.Visible = false;
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            this.Dispose();
            int MAINTENANCE_TYPE = GetReturnWindowCode(plantComponentParent);
            Maintenance newMaintenanceWindow = new Maintenance(MAINTENANCE_TYPE);
            newMaintenanceWindow.Show();
        }

        private int GetReturnWindowCode(PlantComponent plantComponent)
        {
            if (plantComponent.IsDevice())
            {
                return 3;
            }
            return 2;
        }

        private void buttonNew_Click(object sender, EventArgs e)
        {
            VariableData newVariableWindow = new VariableData(plantComponentParent);
            newVariableWindow.ShowDialog();
            LoadData();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (ItemIsSelected())
            {
                Object selectedItem = listBoxVariables.SelectedItem;
                Variable variableToDelete = selectedItem as Variable;
                plantComponentParent.DeleteVariable(variableToDelete);
                textBoxValue.Text = String.Empty;
                MessageBox.Show("The variable was correctly deleted", "Success", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                LoadData();
            }
            else
            {
                MessageBox.Show("You must select an item", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buttonChangeValue_Click(object sender, EventArgs e)
        {
            if (ItemIsSelected())
            {
                textBoxValue.Enabled = true;
                buttonSave.Enabled = true;
                Object selectedItem = listBoxVariables.SelectedItem;
                Variable variableToModify = selectedItem as Variable;
                textBoxValue.Text = variableToModify.ActualValue.ToString();
                String range = "Between " + variableToModify.MinimumValue.ToString() + " and " + variableToModify.MaximumValue.ToString();
                labelRange.Text = range;
                labelRange.Visible = true;
            }
            else
            {
                MessageBox.Show("You must select an item", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            String valueChanged = textBoxValue.Text;
            double newValue;
            bool resultTryParse = double.TryParse(valueChanged, out newValue);
            if (resultTryParse)
            {
                Variable searchVariable = new Variable();
                Object selectedItem = listBoxVariables.SelectedItem;
                Variable variableToModify = selectedItem as Variable;
                searchVariable.Identifier = variableToModify.Identifier;
                searchVariable.CopyDataFrom(variableToModify);
                searchVariable.ActualValue = newValue;
                plantComponentParent.ModifyVariable(searchVariable);
                textBoxValue.Text = String.Empty;
                textBoxValue.Enabled = false;
                buttonSave.Enabled = false;
                labelRange.Visible = false;
                LoadData();
            }
            else
            {
                MessageBox.Show("The new value is invalid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool ItemIsSelected()
        {
            int NO_INDEX_SELECTED = -1;
            if (listBoxVariables.SelectedIndex == NO_INDEX_SELECTED)
            {
                return false;
            }
            return true;
        }

        private void buttonHistoricalValues_Click(object sender, EventArgs e)
        {
            if (ItemIsSelected())
            {
                Object selectedItem = listBoxVariables.SelectedItem;
                Variable variableToReview = selectedItem as Variable;
                HistoricalValues newHistoricalValuesWindow = new HistoricalValues(variableToReview);
                newHistoricalValuesWindow.ShowDialog();
            }
            else
            {
                MessageBox.Show("You must select an item", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
