﻿namespace UI
{
    partial class SelectIncidentReportFilters
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DateTimePicker_DateFilterFrom = new System.Windows.Forms.DateTimePicker();
            this.DateTimePicker_DateFilterTo = new System.Windows.Forms.DateTimePicker();
            this.ComboBox_IncidentSeverityFilter = new System.Windows.Forms.ComboBox();
            this.label_DateFilterFrom = new System.Windows.Forms.Label();
            this.label_DateFilterTo = new System.Windows.Forms.Label();
            this.label_IncidentSeverityFilter = new System.Windows.Forms.Label();
            this.Button_Apply = new System.Windows.Forms.Button();
            this.checkBox_RecursiveSearch = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // DateTimePicker_DateFilterFrom
            // 
            this.DateTimePicker_DateFilterFrom.Location = new System.Drawing.Point(153, 24);
            this.DateTimePicker_DateFilterFrom.Name = "DateTimePicker_DateFilterFrom";
            this.DateTimePicker_DateFilterFrom.Size = new System.Drawing.Size(200, 20);
            this.DateTimePicker_DateFilterFrom.TabIndex = 0;
            // 
            // DateTimePicker_DateFilterTo
            // 
            this.DateTimePicker_DateFilterTo.Location = new System.Drawing.Point(153, 59);
            this.DateTimePicker_DateFilterTo.Name = "DateTimePicker_DateFilterTo";
            this.DateTimePicker_DateFilterTo.Size = new System.Drawing.Size(200, 20);
            this.DateTimePicker_DateFilterTo.TabIndex = 1;
            // 
            // ComboBox_IncidentSeverityFilter
            // 
            this.ComboBox_IncidentSeverityFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBox_IncidentSeverityFilter.FormattingEnabled = true;
            this.ComboBox_IncidentSeverityFilter.Items.AddRange(new object[] {
            "Todos",
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.ComboBox_IncidentSeverityFilter.Location = new System.Drawing.Point(153, 94);
            this.ComboBox_IncidentSeverityFilter.Name = "ComboBox_IncidentSeverityFilter";
            this.ComboBox_IncidentSeverityFilter.Size = new System.Drawing.Size(200, 21);
            this.ComboBox_IncidentSeverityFilter.TabIndex = 2;
            // 
            // label_DateFilterFrom
            // 
            this.label_DateFilterFrom.AutoSize = true;
            this.label_DateFilterFrom.Location = new System.Drawing.Point(18, 27);
            this.label_DateFilterFrom.Name = "label_DateFilterFrom";
            this.label_DateFilterFrom.Size = new System.Drawing.Size(33, 13);
            this.label_DateFilterFrom.TabIndex = 3;
            this.label_DateFilterFrom.Text = "From:";
            // 
            // label_DateFilterTo
            // 
            this.label_DateFilterTo.AutoSize = true;
            this.label_DateFilterTo.Location = new System.Drawing.Point(18, 62);
            this.label_DateFilterTo.Name = "label_DateFilterTo";
            this.label_DateFilterTo.Size = new System.Drawing.Size(23, 13);
            this.label_DateFilterTo.TabIndex = 4;
            this.label_DateFilterTo.Text = "To:";
            // 
            // label_IncidentSeverityFilter
            // 
            this.label_IncidentSeverityFilter.AutoSize = true;
            this.label_IncidentSeverityFilter.Location = new System.Drawing.Point(18, 97);
            this.label_IncidentSeverityFilter.Name = "label_IncidentSeverityFilter";
            this.label_IncidentSeverityFilter.Size = new System.Drawing.Size(45, 13);
            this.label_IncidentSeverityFilter.TabIndex = 5;
            this.label_IncidentSeverityFilter.Text = "Severity";
            // 
            // Button_Apply
            // 
            this.Button_Apply.Location = new System.Drawing.Point(144, 166);
            this.Button_Apply.Name = "Button_Apply";
            this.Button_Apply.Size = new System.Drawing.Size(75, 23);
            this.Button_Apply.TabIndex = 6;
            this.Button_Apply.Text = "Apply Filters";
            this.Button_Apply.UseVisualStyleBackColor = true;
            this.Button_Apply.Click += new System.EventHandler(this.Button_Apply_Click);
            // 
            // checkBox_RecursiveSearch
            // 
            this.checkBox_RecursiveSearch.AutoSize = true;
            this.checkBox_RecursiveSearch.Location = new System.Drawing.Point(153, 129);
            this.checkBox_RecursiveSearch.Name = "checkBox_RecursiveSearch";
            this.checkBox_RecursiveSearch.Size = new System.Drawing.Size(15, 14);
            this.checkBox_RecursiveSearch.TabIndex = 7;
            this.checkBox_RecursiveSearch.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 129);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(130, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Execute recursive search:";
            // 
            // SelectIncidentReportFilters
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(368, 201);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.checkBox_RecursiveSearch);
            this.Controls.Add(this.Button_Apply);
            this.Controls.Add(this.label_IncidentSeverityFilter);
            this.Controls.Add(this.label_DateFilterTo);
            this.Controls.Add(this.label_DateFilterFrom);
            this.Controls.Add(this.ComboBox_IncidentSeverityFilter);
            this.Controls.Add(this.DateTimePicker_DateFilterTo);
            this.Controls.Add(this.DateTimePicker_DateFilterFrom);
            this.Name = "SelectIncidentReportFilters";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select search filters";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker DateTimePicker_DateFilterFrom;
        private System.Windows.Forms.DateTimePicker DateTimePicker_DateFilterTo;
        private System.Windows.Forms.ComboBox ComboBox_IncidentSeverityFilter;
        private System.Windows.Forms.Label label_DateFilterFrom;
        private System.Windows.Forms.Label label_DateFilterTo;
        private System.Windows.Forms.Label label_IncidentSeverityFilter;
        private System.Windows.Forms.Button Button_Apply;
        private System.Windows.Forms.CheckBox checkBox_RecursiveSearch;
        private System.Windows.Forms.Label label1;
    }
}