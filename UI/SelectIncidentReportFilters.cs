﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Domain;

namespace UI
{
    public partial class SelectIncidentReportFilters : Form
    {
        private PlantComponent plantComponent;

        public SelectIncidentReportFilters(PlantComponent plantComponent)
        {
            InitializeComponent();
            this.plantComponent = plantComponent;
            ComboBox_IncidentSeverityFilter.SelectedIndex = 0;
        }

        private void Button_Apply_Click(object sender, EventArgs e)
        {
            DateTime dateFilterFrom = DateTimePicker_DateFilterFrom.Value;
            DateTime dateFilterTo = DateTimePicker_DateFilterTo.Value;
            Tuple<DateTime, DateTime> dateRangeFilters = new Tuple<DateTime, DateTime>(dateFilterFrom, dateFilterTo);
            int severity = 0;
            int INCLUDE_ALL_SEVERITIES_COMBO_OPTION_INDEX = 0;
            if (ComboBox_IncidentSeverityFilter.SelectedIndex != INCLUDE_ALL_SEVERITIES_COMBO_OPTION_INDEX)
            {
                if (ComboBox_IncidentSeverityFilter.SelectedItem != null)
                {
                    severity = int.Parse(ComboBox_IncidentSeverityFilter.SelectedItem.ToString());
                }
            }
            bool executeRecursiveSearch = checkBox_RecursiveSearch.Checked;
            DisplayIncidentsReport incidentsReport = new DisplayIncidentsReport(dateRangeFilters, severity, plantComponent, executeRecursiveSearch);
            incidentsReport.Show();
            this.Dispose();
        }
    }
}
