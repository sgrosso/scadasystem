﻿using Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI
{
    public partial class HistoricalValues : Form
    {
        private Variable variableToReview;
        public HistoricalValues(Variable variable)
        {
            InitializeComponent();
            variableToReview = variable;
            LoadData();
        }

        private void LoadData()
        {
            listBoxValues.Items.AddRange(ListHistoricalValues().ToArray());
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private List<String> ListHistoricalValues()
        {
            List<String> historicalValues = new List<String>();
            foreach(var value in variableToReview.HistoricalValues)
            {
                String toAdd = "Date and Time: ";
                toAdd = toAdd + value.Item2.ToString();
                toAdd = toAdd + " - Value: ";
                toAdd = toAdd + value.Item1.ToString();
                historicalValues.Add(toAdd);
            }
            return historicalValues;
        }
    }
}
