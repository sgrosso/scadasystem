﻿using Domain;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace UI
{
    public partial class InstallationView : Form
    {
        private Installation installationToModify;

        public InstallationView(Installation newInstallation)
        {
            InitializeComponent();
            installationToModify = newInstallation;
            LoadData();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            int MAINTENANCE_TYPE = 2;
            this.Dispose();
            Maintenance newMaintenanceWindow = new Maintenance(MAINTENANCE_TYPE);
            newMaintenanceWindow.Show();
        }

        private void LoadData()
        {
            listBoxChildren.Items.Clear();
            listBoxComponents.Items.Clear();
            listBoxComponents.Items.AddRange(CandidatesChildren().ToArray());
            listBoxChildren.Items.AddRange(installationToModify.ChildrenComponents.ToArray());
            if (installationToModify.IsEmpty())
            {
                labelTitle.Text = "New Installation";
            }
            else
            {
                labelTitle.Text = "Modify Installation";
                textBoxName.Text = installationToModify.Name;
            }
        }

        private List<PlantComponent> CandidatesChildren()
        {
            List<PlantComponent> candidatesChildren = new List<PlantComponent>();
            foreach(object component in ProductionPlant.GetInstance().ListPlantComponents())
            {
                PlantComponent plantComponent = component as PlantComponent;
                if (!plantComponent.IsUsed() && installationToModify.Identifier!=plantComponent.Identifier)
                {
                    candidatesChildren.Add(plantComponent);
                }
            }
            return candidatesChildren;
        }
        private void buttonToChidren_Click(object sender, EventArgs e)
        {
            int NO_SELECTION_INDEX = -1;
            if (listBoxComponents.SelectedIndex != NO_SELECTION_INDEX)
            {
                Object selectedItem = listBoxComponents.SelectedItem;
                listBoxChildren.Items.Add(selectedItem);
                listBoxComponents.Items.Remove(selectedItem);
            }
            else
            {
                MessageBox.Show("You must select an item","Warning",MessageBoxButtons.OK,MessageBoxIcon.Warning);
            }
        }

        private void buttonDeleteChildren_Click(object sender, EventArgs e)
        {
            int NO_SELECTION_INDEX = -1;
            if (listBoxChildren.SelectedIndex != NO_SELECTION_INDEX)
            {
                Object selectedItem = listBoxChildren.SelectedItem;
                PlantComponent plantComponent = selectedItem as PlantComponent;
                listBoxComponents.Items.Add(plantComponent);
                listBoxChildren.Items.Remove(plantComponent);
            }
            else
            {
                MessageBox.Show("You must select an item", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            if (installationToModify.IsEmpty())
            {
                Newinstallation();
            }
            else
            {
                ModifyInstallation();
            }
            CandidatesChildrenSetNotInUse();
        }

        private List<PlantComponent> ChildrenComponentsFromListBox()
        {
            List<PlantComponent> newChildrenComponentList = new List<PlantComponent>();
            foreach (object listItem in listBoxChildren.Items)
            {
                PlantComponent component = listItem as PlantComponent;
                component.SetInUseBy(installationToModify);
                newChildrenComponentList.Add(component);
            }
            return newChildrenComponentList;
        } 

        private void CandidatesChildrenSetNotInUse()
        {
            foreach(object listItem in listBoxComponents.Items)
            {
                PlantComponent component = listItem as PlantComponent;
                component.SetNotInUse();
            }
        }

        private void Newinstallation()
        {
            String installationName = textBoxName.Text.Trim();
            if (String.IsNullOrEmpty(installationName))
            {
                MessageBox.Show("You must type a name", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                installationToModify.ChildrenComponents = null;
                installationToModify.ChildrenComponents = ChildrenComponentsFromListBox();
                installationToModify.Name = installationName;
                bool wasCorrectlyAdded = ProductionPlant.GetInstance().AddInstallation(installationToModify);
                if (wasCorrectlyAdded)
                {
                    MessageBox.Show("Installation correctly added", "Success", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    this.Dispose();
                    int MAINTENANCE_TYPE = 2;
                    Maintenance newMaintenanceWindow = new Maintenance(MAINTENANCE_TYPE);
                    newMaintenanceWindow.Show();
                }
                else
                {
                    MessageBox.Show("The installation was already added", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void ModifyInstallation()
        {
            String installationName = textBoxName.Text.Trim();
            if (String.IsNullOrEmpty(installationName))
            {
                MessageBox.Show("You must type a name", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                DeleteAllChildrenComponents(installationToModify);
                installationToModify.ChildrenComponents = null;
                installationToModify.ChildrenComponents = ChildrenComponentsFromListBox();
                installationToModify.Name = installationName;
                bool wasCorrectlyModified = ProductionPlant.GetInstance().ModifyInstallation(installationToModify);
                if (wasCorrectlyModified)
                {
                    MessageBox.Show("The installation was correctly modified", "Success", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    this.Dispose();
                    int MAINTENANCE_TYPE = 2;
                    Maintenance newMaintenanceWindow = new Maintenance(MAINTENANCE_TYPE);
                    newMaintenanceWindow.Show();
                }
                else
                {
                    MessageBox.Show("The installation wasn't modified", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void DeleteAllChildrenComponents(Installation installation)
        {
            foreach(var aComponent in installationToModify.ChildrenComponents)
            {
                aComponent.Parent = null;
            }
        }
    }
}

