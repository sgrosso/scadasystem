﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Domain;
namespace UI
{
    public partial class Maintenance : Form
    {
        private int maintenanceType;
        public Maintenance(int aMaintenanceType)
        {
            InitializeComponent();
            maintenanceType = aMaintenanceType;
            LoadData();
        }

        private bool itemIsSelected()
        {
            int NO_SELECTION_INDEX = -1;
            if(listBoxPlantComponent.SelectedIndex == NO_SELECTION_INDEX)
            {
                return false;
            }
            return true;
        }

        private void NewButton_Click(object sender, EventArgs e)
        {
            if (maintenanceType == 1)
            {
                CreateNewIndustrialPlant();
            }
            else if(maintenanceType == 2)
            {
                CreateNewInstallation();
            }
            else
            {
                CreateNewDevice();
            }

        }

        private void LoadData()
        {
            if (maintenanceType == 1)
            {
                LoadIndustrialPlantData();
            }
            else if(maintenanceType == 2)
            {
                LoadInstallationData();
            }
            else
            {
                LoadDeviceData();
            }
        }

        private void LoadIndustrialPlantData()
        {
            TitleLabel.Text = "Industrial Plant Maintenance";
            listBoxPlantComponent.Items.Clear();
            listBoxPlantComponent.Items.AddRange(ProductionPlant.GetInstance().ListIndustrialPlants().ToArray());
            VariablesButton.Enabled = false;
        }
        private void LoadInstallationData()
        {
            TitleLabel.Text = "Installation Maintenance";
            listBoxPlantComponent.Items.Clear();
            listBoxPlantComponent.Items.AddRange(ProductionPlant.GetInstance().ListInstallations().ToArray());
            VariablesButton.Enabled = true;
        }

        private void LoadDeviceData()
        {
            TitleLabel.Text = "Device Maintenance";
            listBoxPlantComponent.Items.Clear();
            listBoxPlantComponent.Items.AddRange(ProductionPlant.GetInstance().ListDevices().ToArray());
            VariablesButton.Enabled = true;
        }

        private void CreateNewIndustrialPlant()
        {
            IndustrialPlant industrialPlant = new IndustrialPlant();
            IndustrialPlantView newIndustrialPlantWindow = new IndustrialPlantView(industrialPlant);
            this.Dispose();
            newIndustrialPlantWindow.Show();
        }

        private void CreateNewInstallation()
        {
            Installation installation = new Installation();
            InstallationView newInstallationWindow = new InstallationView(installation);
            this.Dispose();
            newInstallationWindow.Show();
        }

        private void CreateNewDevice()
        {
            Device newDevice = new Device();
            DeviceView newDeviceWindow = new DeviceView(newDevice);
            this.Dispose();
            newDeviceWindow.Show();
        }

        private void ModifyButton_Click(object sender, EventArgs e)
        {
            if (maintenanceType == 1)
            {
                ModifyIndustrialPlant();
            }
            else if(maintenanceType == 2)
            {
                ModifyInstallation();
            }
            else
            {
                ModifyDevice();
            }
        }

        private void ModifyIndustrialPlant()
        {
            if (itemIsSelected())
            {
                Object selectedItem = listBoxPlantComponent.SelectedItem;
                IndustrialPlant industrialPlantToModify = selectedItem as IndustrialPlant;
                IndustrialPlantView newIndustrialPlantWindow = new IndustrialPlantView(industrialPlantToModify);
                this.Dispose();
                newIndustrialPlantWindow.Show();
            }
            else
            {
                MessageBox.Show("You must select an industrial plant", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void ModifyInstallation()
        {

            if (itemIsSelected())
            {
                Object selectedItem = listBoxPlantComponent.SelectedItem;
                Installation installationToModify = selectedItem as Installation;
                InstallationView newInstallationModifyWindow = new InstallationView(installationToModify);
                this.Dispose();
                newInstallationModifyWindow.Show();
            }
            else
            {
                MessageBox.Show("You must select an installation", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void ModifyDevice()
        {
            if (itemIsSelected())
            {
                Object selectedItem = listBoxPlantComponent.SelectedItem;
                Device deviceToModify = selectedItem as Device;
                DeviceView newDeviceViewWindow = new DeviceView(deviceToModify);
                this.Dispose();
                newDeviceViewWindow.Show();
            }
            else
            {
                MessageBox.Show("You must select a device", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            if (maintenanceType == 1)
            {
                DeleteIndustrialPlant();
            }
            else if(maintenanceType == 2)
            {
                DeleteInstallation();
            }
            else
            {
                DeleteDevice();
            }
        }

        private void DeleteIndustrialPlant()
        {
            if (itemIsSelected())
            {
                Object selectedItem = listBoxPlantComponent.SelectedItem;
                IndustrialPlant industrialPlantToDelete = selectedItem as IndustrialPlant;
                bool isCorrectlyDeleted = ProductionPlant.GetInstance().DeleteIndustrialPlant(industrialPlantToDelete);
                if (isCorrectlyDeleted)
                {
                    if(industrialPlantToDelete.Parent != null)
                    {
                        industrialPlantToDelete.Parent.DeleteComponent(industrialPlantToDelete);
                    }
                    MessageBox.Show("The industrial plant was correctly deleted", "Success", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    LoadIndustrialPlantData();
                }
                else
                {
                    MessageBox.Show("The industrial plant doesn't exists", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("You must select an industrial plant", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void DeleteInstallation()
        {
            if (itemIsSelected())
            {
                Object selectedItem = listBoxPlantComponent.SelectedItem;
                Installation installationToDelete = selectedItem as Installation;
                bool isCorrectlyDeleted = ProductionPlant.GetInstance().DeleteInstallation(installationToDelete);
                if (isCorrectlyDeleted)
                {
                    if (installationToDelete.Parent != null)
                    {
                        installationToDelete.Parent.DeleteComponent(installationToDelete);
                    }                 
                    MessageBox.Show("The installation was correctly deleted", "Success", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    LoadInstallationData();
                }
                else
                {
                    MessageBox.Show("The installation doesn't exists", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("You must select an installation", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void DeleteDevice()
        {
            if (itemIsSelected())
            {
                Object selectedItem = listBoxPlantComponent.SelectedItem;
                Domain.Device deviceToDelete = selectedItem as Domain.Device;
                bool isCorrectlyDeleted = ProductionPlant.GetInstance().DeleteDevice(deviceToDelete);
                if (isCorrectlyDeleted)
                {
                    if (deviceToDelete.Parent != null)
                    {
                        deviceToDelete.Parent.DeleteComponent(deviceToDelete);
                    }
                    MessageBox.Show("The device was correctly deleted", "Success", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    LoadDeviceData();
                }
                else
                {
                    MessageBox.Show("The device doesn't exists", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("You must select a device", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void VariablesButton_Click(object sender, EventArgs e)
        {
            if (itemIsSelected())
            {
                NewVariableWindow();
            }
            else
            {
                MessageBox.Show("You must select an item", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void NewVariableWindow()
        {
            VariableView newVariableViewWindow;
            Object selectedItem = listBoxPlantComponent.SelectedItem;
            if(maintenanceType == 2)
            { 
                Installation installation = selectedItem as Installation;
                newVariableViewWindow = new VariableView(installation);
            }
            else
            {
                Domain.Device device = selectedItem as Domain.Device;
                newVariableViewWindow = new VariableView(device);
            }
            this.Dispose();
            newVariableViewWindow.Show();
        }

        private void BackButton_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void Button_ReportIncident_Click(object sender, EventArgs e)
        {
            if (itemIsSelected())
            {
                PlantComponent selectedPlantComponent = listBoxPlantComponent.SelectedItem as PlantComponent;
                NewPlantComponentIncident reportNewIncidentWindow = new NewPlantComponentIncident(selectedPlantComponent);
                reportNewIncidentWindow.Show();
            }
            else
            {
                MessageBox.Show("You must select an item", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void Button_ViewIncidents_Click(object sender, EventArgs e)
        {
            if (itemIsSelected())
            {
                PlantComponent selectedPlantComponent = listBoxPlantComponent.SelectedItem as PlantComponent;
                SelectIncidentReportFilters filtersSelection = new SelectIncidentReportFilters(selectedPlantComponent);
                filtersSelection.Show();
            }
            else
            {
                MessageBox.Show("You must select an item", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}
