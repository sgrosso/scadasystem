﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Domain;
using Persistence;
using DataBasePersistence;
using IncidentReporting;

namespace UI
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            //DomainPersistence repository = MemoryPersistenceMainRepository.GetMainRepostoryInstance();
            DomainPersistence repository = new DataBaseQueries();
            ProductionPlant.GetInstance().Repository = repository;
            IncidentReporter reportingMethod;
            IncidentLoader loadingMethod;
            String actualIncidentReportingMethod = Properties.Settings.Default.IncidentReportingMethod;
            if (actualIncidentReportingMethod == "TXT")
            {
                reportingMethod = new IncidentTextFileReporter();
                loadingMethod = new IncidentTextFileLoader();
            }
            else
            {
                reportingMethod = new IncidentDatabaseReporter();
                loadingMethod = new IncidentDatabaseLoader();
            }
            ProductionPlant.GetInstance().SetIncidentReportingMethod(reportingMethod);
            ProductionPlant.GetInstance().SetIncidentLoadingMethod(loadingMethod);            
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainMenu());
        }
    }
}
