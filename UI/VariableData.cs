﻿using Domain;
using System;
using System.Windows.Forms;

namespace UI
{
    public partial class VariableData : Form
    {
        PlantComponent plantComponentParent;
        public VariableData(PlantComponent plantComponent)
        {
            InitializeComponent();
            plantComponentParent = plantComponent;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            if (ThereAreEmptyComponents())
            {
                MessageBox.Show("You must fill all the fields", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (AllValuesAreNumbers())
                {
                    if (ValueIsInRange())
                    {
                        if (MaximumIsLessThanMinimum())
                        {
                            MessageBox.Show("Minimum must be less than maximum", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            if (WarningValuesAreBetweenAlarmValues())
                            {
                                Variable newVariable = CreateNewVariable();
                                plantComponentParent.AddVariable(newVariable);
                                this.Dispose();
                            }
                            else
                            {
                                MessageBox.Show("Warning values must be between alarm values", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        
                    }
                    else
                    {
                        MessageBox.Show("Actual value must be a value between minimum and maximum", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("You must fill the values with numbers", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private bool ThereAreEmptyComponents()
        {
            String name = textBoxName.Text;
            String maximum = textBoxMaximum.Text;
            String minimum = textBoxMinimum.Text;
            String maximumWarning = textBoxMaximumWarning.Text;
            String minimumWarning = textBoxMinimumWarning.Text;
            String value = textBoxValue.Text;
            return String.IsNullOrEmpty(name) || String.IsNullOrEmpty(maximum) || String.IsNullOrEmpty(minimum) || 
                String.IsNullOrEmpty(maximumWarning) || String.IsNullOrEmpty(minimumWarning) || String.IsNullOrEmpty(value);
        }
        
        private Variable CreateNewVariable()
        {
            double newMaximum = Parser(textBoxMaximum.Text).Item2;
            double newMinimum = Parser(textBoxMinimum.Text).Item2;
            double newMaximumWarning = Parser(textBoxMaximumWarning.Text).Item2;
            double newMinimumWarning = Parser(textBoxMinimumWarning.Text).Item2;
            double newValue = Parser(textBoxValue.Text).Item2;
            Variable newVariable = new Variable()
            {
                Name = textBoxName.Text.Trim(),
                MaximumValue = newMaximum,
                MinimumValue = newMinimum,
                MinimumWarningValue = newMinimumWarning,
                MaximumWarningValue = newMaximumWarning,
                ActualValue = newValue
            };
            return newVariable;
        }

        private bool AllValuesAreNumbers()
        {

            bool maximumOk = Parser(textBoxMaximum.Text).Item1;
            bool minimumOk = Parser(textBoxMinimum.Text).Item1;
            bool maximumWarningOK = Parser(textBoxMaximumWarning.Text).Item1;
            bool minimumWarningOK = Parser(textBoxMinimumWarning.Text).Item1;
            bool valueOk = Parser(textBoxValue.Text).Item1;

            return maximumOk && minimumOk && maximumWarningOK && minimumWarningOK && valueOk;
        }

        private bool ValueIsInRange()
        {
            double newMaximumWarning = Parser(textBoxMaximumWarning.Text).Item2;
            double newMinimumWarning = Parser(textBoxMinimumWarning.Text).Item2;
            double newValue;

            bool valueOk = double.TryParse(textBoxValue.Text, out newValue);

            return newValue >= newMinimumWarning && newValue <= newMaximumWarning;
        }

        private bool MaximumIsLessThanMinimum()
        {
            double newMaximum = Parser(textBoxMaximum.Text).Item2;
            double newMinimum = Parser(textBoxMinimum.Text).Item2;
            double newMaximunWarning = Parser(textBoxMaximumWarning.Text).Item2;
            double newMinimumWarning = Parser(textBoxMinimumWarning.Text).Item2;

            return newMaximum < newMinimum || newMaximunWarning < newMinimumWarning;
        }

        private bool WarningValuesAreBetweenAlarmValues()
        {
            double newMaximum = Parser(textBoxMaximum.Text).Item2;
            double newMinimum = Parser(textBoxMinimum.Text).Item2;
            double newMaximumWarning = Parser(textBoxMaximumWarning.Text).Item2;
            double newMinimumWarning = Parser(textBoxMinimumWarning.Text).Item2;

            return newMaximum > newMaximumWarning && newMaximum > newMinimumWarning && 
                newMinimum < newMaximumWarning && newMinimum < newMinimumWarning;
        }

        private Tuple<bool, double> Parser(String stringToParse)
        {
            double valueParsed;
            bool wasCorrectlyParsed = double.TryParse(stringToParse, out valueParsed);
            return new Tuple<bool, double>(wasCorrectlyParsed, valueParsed);
        }
    }
}
