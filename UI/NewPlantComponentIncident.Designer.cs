﻿namespace UI
{
    partial class NewPlantComponentIncident
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_IncidentSeverity = new System.Windows.Forms.Label();
            this.lable_IncidentDescription = new System.Windows.Forms.Label();
            this.richTextBox_IncidentDescription = new System.Windows.Forms.RichTextBox();
            this.dateTimePicker_IncidentDate = new System.Windows.Forms.DateTimePicker();
            this.label_IncidentDate = new System.Windows.Forms.Label();
            this.comboBox_IncidentSeverity = new System.Windows.Forms.ComboBox();
            this.Button_Ok = new System.Windows.Forms.Button();
            this.ButtonCancel = new System.Windows.Forms.Button();
            this.label_IncidentPlantComponent = new System.Windows.Forms.Label();
            this.label_PlantComponentIdentifier = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label_IncidentSeverity
            // 
            this.label_IncidentSeverity.AutoSize = true;
            this.label_IncidentSeverity.Location = new System.Drawing.Point(18, 116);
            this.label_IncidentSeverity.Name = "label_IncidentSeverity";
            this.label_IncidentSeverity.Size = new System.Drawing.Size(48, 13);
            this.label_IncidentSeverity.TabIndex = 0;
            this.label_IncidentSeverity.Text = "Severity:";
            // 
            // lable_IncidentDescription
            // 
            this.lable_IncidentDescription.AutoSize = true;
            this.lable_IncidentDescription.Location = new System.Drawing.Point(18, 154);
            this.lable_IncidentDescription.Name = "lable_IncidentDescription";
            this.lable_IncidentDescription.Size = new System.Drawing.Size(99, 13);
            this.lable_IncidentDescription.TabIndex = 1;
            this.lable_IncidentDescription.Text = "Incident description";
            // 
            // richTextBox_IncidentDescription
            // 
            this.richTextBox_IncidentDescription.Location = new System.Drawing.Point(21, 181);
            this.richTextBox_IncidentDescription.Name = "richTextBox_IncidentDescription";
            this.richTextBox_IncidentDescription.Size = new System.Drawing.Size(326, 116);
            this.richTextBox_IncidentDescription.TabIndex = 9;
            this.richTextBox_IncidentDescription.Text = "";
            // 
            // dateTimePicker_IncidentDate
            // 
            this.dateTimePicker_IncidentDate.Location = new System.Drawing.Point(138, 81);
            this.dateTimePicker_IncidentDate.Name = "dateTimePicker_IncidentDate";
            this.dateTimePicker_IncidentDate.Size = new System.Drawing.Size(209, 20);
            this.dateTimePicker_IncidentDate.TabIndex = 10;
            // 
            // label_IncidentDate
            // 
            this.label_IncidentDate.AutoSize = true;
            this.label_IncidentDate.Location = new System.Drawing.Point(19, 85);
            this.label_IncidentDate.Name = "label_IncidentDate";
            this.label_IncidentDate.Size = new System.Drawing.Size(98, 13);
            this.label_IncidentDate.TabIndex = 11;
            this.label_IncidentDate.Text = "Date of Ocurrence:";
            // 
            // comboBox_IncidentSeverity
            // 
            this.comboBox_IncidentSeverity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_IncidentSeverity.FormattingEnabled = true;
            this.comboBox_IncidentSeverity.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.comboBox_IncidentSeverity.Location = new System.Drawing.Point(138, 110);
            this.comboBox_IncidentSeverity.Name = "comboBox_IncidentSeverity";
            this.comboBox_IncidentSeverity.Size = new System.Drawing.Size(209, 21);
            this.comboBox_IncidentSeverity.TabIndex = 12;
            // 
            // Button_Ok
            // 
            this.Button_Ok.Location = new System.Drawing.Point(93, 303);
            this.Button_Ok.Name = "Button_Ok";
            this.Button_Ok.Size = new System.Drawing.Size(75, 23);
            this.Button_Ok.TabIndex = 13;
            this.Button_Ok.Text = "OK";
            this.Button_Ok.UseVisualStyleBackColor = true;
            this.Button_Ok.Click += new System.EventHandler(this.Button_Ok_Click);
            // 
            // ButtonCancel
            // 
            this.ButtonCancel.Location = new System.Drawing.Point(196, 303);
            this.ButtonCancel.Name = "ButtonCancel";
            this.ButtonCancel.Size = new System.Drawing.Size(75, 23);
            this.ButtonCancel.TabIndex = 14;
            this.ButtonCancel.Text = "Cancel";
            this.ButtonCancel.UseVisualStyleBackColor = true;
            this.ButtonCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // label_IncidentPlantComponent
            // 
            this.label_IncidentPlantComponent.AutoSize = true;
            this.label_IncidentPlantComponent.Location = new System.Drawing.Point(19, 35);
            this.label_IncidentPlantComponent.Name = "label_IncidentPlantComponent";
            this.label_IncidentPlantComponent.Size = new System.Drawing.Size(111, 13);
            this.label_IncidentPlantComponent.TabIndex = 15;
            this.label_IncidentPlantComponent.Text = "Reporting incident for:";
            // 
            // label_PlantComponentIdentifier
            // 
            this.label_PlantComponentIdentifier.AutoSize = true;
            this.label_PlantComponentIdentifier.Location = new System.Drawing.Point(138, 35);
            this.label_PlantComponentIdentifier.Name = "label_PlantComponentIdentifier";
            this.label_PlantComponentIdentifier.Size = new System.Drawing.Size(45, 13);
            this.label_PlantComponentIdentifier.TabIndex = 16;
            this.label_PlantComponentIdentifier.Text = "Incident";
            // 
            // NewPlantComponentIncident
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(370, 351);
            this.Controls.Add(this.label_PlantComponentIdentifier);
            this.Controls.Add(this.label_IncidentPlantComponent);
            this.Controls.Add(this.ButtonCancel);
            this.Controls.Add(this.Button_Ok);
            this.Controls.Add(this.comboBox_IncidentSeverity);
            this.Controls.Add(this.label_IncidentDate);
            this.Controls.Add(this.dateTimePicker_IncidentDate);
            this.Controls.Add(this.richTextBox_IncidentDescription);
            this.Controls.Add(this.lable_IncidentDescription);
            this.Controls.Add(this.label_IncidentSeverity);
            this.Name = "NewPlantComponentIncident";
            this.Text = "New Incident";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_IncidentSeverity;
        private System.Windows.Forms.Label lable_IncidentDescription;
        private System.Windows.Forms.RichTextBox richTextBox_IncidentDescription;
        private System.Windows.Forms.DateTimePicker dateTimePicker_IncidentDate;
        private System.Windows.Forms.Label label_IncidentDate;
        private System.Windows.Forms.ComboBox comboBox_IncidentSeverity;
        private System.Windows.Forms.Button Button_Ok;
        private System.Windows.Forms.Button ButtonCancel;
        private System.Windows.Forms.Label label_IncidentPlantComponent;
        private System.Windows.Forms.Label label_PlantComponentIdentifier;
    }
}