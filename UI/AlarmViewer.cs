﻿using Domain;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace UI
{
    public partial class AlarmViewer : UserControl
    {
        private PlantComponent component;
        private bool activated;
        private bool hasWarnings;
        public AlarmViewer(PlantComponent plantComponent)
        {
            component = plantComponent;
            activated = plantComponent.OutOfRangeVariablesNumber > 0;
            hasWarnings = plantComponent.OutOfWarningRangeVariablesNumber > 0;
            InitializeComponent();
            LoadControls();
            
        }

        private void LoadControls()
        {
            Image alarmImage = GetAlarmImageBasedOnPlantComponentsAlarms();
            picBox_AlarmIcon.Image = alarmImage;
            picBox_AlarmIcon.SizeMode = PictureBoxSizeMode.StretchImage;
            lbl_ComponentData.Text = component.Name;
            label_AlarmsNumber.Text = component.OutOfRangeVariablesNumber.ToString();
            label_WarningsNumber.Text = component.OutOfWarningRangeVariablesNumber.ToString();
            int alarmsQuantity = component.OutOfWarningRangeVariablesNumber + component.OutOfWarningRangeVariablesNumber;
            Button_DrillDown.Visible = (alarmsQuantity != 0);
        }

        private Image GetAlarmImageBasedOnPlantComponentsAlarms()
        {
            Image alarmImage;
            if (activated)
            {
                alarmImage = UI.Properties.Resources.AlarmActivated;
                return alarmImage;
            }
            else
            {
                if (hasWarnings)
                {
                    alarmImage = UI.Properties.Resources.Warning;
                    return alarmImage;
                }
            }

            alarmImage = UI.Properties.Resources.AlarmUnactivated;
            return alarmImage;
        }

        public void UpdateControl(PlantComponent plantComponent)
        {
            component = plantComponent;
            activated = component.OutOfRangeVariablesNumber > 0;
            hasWarnings = component.OutOfWarningRangeVariablesNumber > 0;
            LoadControls();
        }
    }
}
