﻿namespace UI
{
    partial class MainMenu
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.MaintenanceDevicesButton = new System.Windows.Forms.Button();
            this.MaintenanceInstallationButton = new System.Windows.Forms.Button();
            this.Button_ControlPanel = new System.Windows.Forms.Button();
            this.labelTitle = new System.Windows.Forms.Label();
            this.maintenanceIndustrialPlantButton = new System.Windows.Forms.Button();
            this.Button_SetReportMethodConfiguration = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // MaintenanceDevicesButton
            // 
            this.MaintenanceDevicesButton.Location = new System.Drawing.Point(66, 72);
            this.MaintenanceDevicesButton.Name = "MaintenanceDevicesButton";
            this.MaintenanceDevicesButton.Size = new System.Drawing.Size(159, 34);
            this.MaintenanceDevicesButton.TabIndex = 0;
            this.MaintenanceDevicesButton.Text = "Maintenance of Devices";
            this.MaintenanceDevicesButton.UseVisualStyleBackColor = true;
            this.MaintenanceDevicesButton.Click += new System.EventHandler(this.MaintenanceDevicesButton_Click);
            // 
            // MaintenanceInstallationButton
            // 
            this.MaintenanceInstallationButton.Location = new System.Drawing.Point(66, 126);
            this.MaintenanceInstallationButton.Name = "MaintenanceInstallationButton";
            this.MaintenanceInstallationButton.Size = new System.Drawing.Size(159, 34);
            this.MaintenanceInstallationButton.TabIndex = 1;
            this.MaintenanceInstallationButton.Text = "Maintenance of Installations";
            this.MaintenanceInstallationButton.UseVisualStyleBackColor = true;
            this.MaintenanceInstallationButton.Click += new System.EventHandler(this.MaintenanceInstallationButton_Click);
            // 
            // Button_ControlPanel
            // 
            this.Button_ControlPanel.Location = new System.Drawing.Point(66, 229);
            this.Button_ControlPanel.Name = "Button_ControlPanel";
            this.Button_ControlPanel.Size = new System.Drawing.Size(159, 34);
            this.Button_ControlPanel.TabIndex = 3;
            this.Button_ControlPanel.Text = "Control Panel";
            this.Button_ControlPanel.UseVisualStyleBackColor = true;
            this.Button_ControlPanel.Click += new System.EventHandler(this.Button_ControlPanel_Click);
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitle.Location = new System.Drawing.Point(51, 9);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(189, 31);
            this.labelTitle.TabIndex = 3;
            this.labelTitle.Text = "Scada System";
            // 
            // maintenanceIndustrialPlantButton
            // 
            this.maintenanceIndustrialPlantButton.Location = new System.Drawing.Point(66, 180);
            this.maintenanceIndustrialPlantButton.Name = "maintenanceIndustrialPlantButton";
            this.maintenanceIndustrialPlantButton.Size = new System.Drawing.Size(159, 34);
            this.maintenanceIndustrialPlantButton.TabIndex = 2;
            this.maintenanceIndustrialPlantButton.Text = "Maintenance of Industrial Plants";
            this.maintenanceIndustrialPlantButton.UseVisualStyleBackColor = true;
            this.maintenanceIndustrialPlantButton.Click += new System.EventHandler(this.maintenanceIndustrialPlantButton_Click);
            // 
            // Button_SetReportMethodConfiguration
            // 
            this.Button_SetReportMethodConfiguration.Location = new System.Drawing.Point(66, 278);
            this.Button_SetReportMethodConfiguration.Name = "Button_SetReportMethodConfiguration";
            this.Button_SetReportMethodConfiguration.Size = new System.Drawing.Size(159, 34);
            this.Button_SetReportMethodConfiguration.TabIndex = 4;
            this.Button_SetReportMethodConfiguration.Text = "Incident reporting methods";
            this.Button_SetReportMethodConfiguration.UseVisualStyleBackColor = true;
            this.Button_SetReportMethodConfiguration.Click += new System.EventHandler(this.Button_SetReportMethodConfiguration_Click);
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 349);
            this.Controls.Add(this.Button_SetReportMethodConfiguration);
            this.Controls.Add(this.maintenanceIndustrialPlantButton);
            this.Controls.Add(this.labelTitle);
            this.Controls.Add(this.Button_ControlPanel);
            this.Controls.Add(this.MaintenanceInstallationButton);
            this.Controls.Add(this.MaintenanceDevicesButton);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Scada System";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button MaintenanceDevicesButton;
        private System.Windows.Forms.Button MaintenanceInstallationButton;
        private System.Windows.Forms.Button Button_ControlPanel;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Button maintenanceIndustrialPlantButton;
        private System.Windows.Forms.Button Button_SetReportMethodConfiguration;
    }
}

