﻿namespace UI
{
    partial class VariableData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelTitle = new System.Windows.Forms.Label();
            this.labelName = new System.Windows.Forms.Label();
            this.labelMaximum = new System.Windows.Forms.Label();
            this.labelMinimum = new System.Windows.Forms.Label();
            this.labelActual = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.textBoxMaximum = new System.Windows.Forms.TextBox();
            this.textBoxMinimum = new System.Windows.Forms.TextBox();
            this.textBoxValue = new System.Windows.Forms.TextBox();
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.label_MaximumWarningValue = new System.Windows.Forms.Label();
            this.label_MinimumWarningValue = new System.Windows.Forms.Label();
            this.textBoxMaximumWarning = new System.Windows.Forms.TextBox();
            this.textBoxMinimumWarning = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitle.Location = new System.Drawing.Point(76, 9);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(175, 31);
            this.labelTitle.TabIndex = 0;
            this.labelTitle.Text = "New Variable";
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(53, 79);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(38, 13);
            this.labelName.TabIndex = 1;
            this.labelName.Text = "Name:";
            // 
            // labelMaximum
            // 
            this.labelMaximum.AutoSize = true;
            this.labelMaximum.Location = new System.Drawing.Point(53, 112);
            this.labelMaximum.Name = "labelMaximum";
            this.labelMaximum.Size = new System.Drawing.Size(54, 13);
            this.labelMaximum.TabIndex = 2;
            this.labelMaximum.Text = "Maximum:";
            // 
            // labelMinimum
            // 
            this.labelMinimum.AutoSize = true;
            this.labelMinimum.Location = new System.Drawing.Point(53, 145);
            this.labelMinimum.Name = "labelMinimum";
            this.labelMinimum.Size = new System.Drawing.Size(51, 13);
            this.labelMinimum.TabIndex = 3;
            this.labelMinimum.Text = "Minimum:";
            // 
            // labelActual
            // 
            this.labelActual.AutoSize = true;
            this.labelActual.Location = new System.Drawing.Point(53, 249);
            this.labelActual.Name = "labelActual";
            this.labelActual.Size = new System.Drawing.Size(70, 13);
            this.labelActual.TabIndex = 4;
            this.labelActual.Text = "Actual Value:";
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(183, 76);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(138, 20);
            this.textBoxName.TabIndex = 1;
            // 
            // textBoxMaximum
            // 
            this.textBoxMaximum.Location = new System.Drawing.Point(183, 109);
            this.textBoxMaximum.Name = "textBoxMaximum";
            this.textBoxMaximum.Size = new System.Drawing.Size(49, 20);
            this.textBoxMaximum.TabIndex = 2;
            // 
            // textBoxMinimum
            // 
            this.textBoxMinimum.Location = new System.Drawing.Point(183, 142);
            this.textBoxMinimum.Name = "textBoxMinimum";
            this.textBoxMinimum.Size = new System.Drawing.Size(49, 20);
            this.textBoxMinimum.TabIndex = 3;
            // 
            // textBoxValue
            // 
            this.textBoxValue.Location = new System.Drawing.Point(183, 246);
            this.textBoxValue.Name = "textBoxValue";
            this.textBoxValue.Size = new System.Drawing.Size(49, 20);
            this.textBoxValue.TabIndex = 6;
            // 
            // buttonOk
            // 
            this.buttonOk.Location = new System.Drawing.Point(82, 303);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(75, 23);
            this.buttonOk.TabIndex = 7;
            this.buttonOk.Text = "OK";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(210, 303);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 8;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // label_MaximumWarningValue
            // 
            this.label_MaximumWarningValue.AutoSize = true;
            this.label_MaximumWarningValue.Location = new System.Drawing.Point(53, 180);
            this.label_MaximumWarningValue.Name = "label_MaximumWarningValue";
            this.label_MaximumWarningValue.Size = new System.Drawing.Size(127, 13);
            this.label_MaximumWarningValue.TabIndex = 11;
            this.label_MaximumWarningValue.Text = "Maximum Warning Value:";
            // 
            // label_MinimumWarningValue
            // 
            this.label_MinimumWarningValue.AutoSize = true;
            this.label_MinimumWarningValue.Location = new System.Drawing.Point(53, 214);
            this.label_MinimumWarningValue.Name = "label_MinimumWarningValue";
            this.label_MinimumWarningValue.Size = new System.Drawing.Size(124, 13);
            this.label_MinimumWarningValue.TabIndex = 12;
            this.label_MinimumWarningValue.Text = "Minimum Warning Value:";
            // 
            // textBoxMaximumWarning
            // 
            this.textBoxMaximumWarning.Location = new System.Drawing.Point(183, 177);
            this.textBoxMaximumWarning.Name = "textBoxMaximumWarning";
            this.textBoxMaximumWarning.Size = new System.Drawing.Size(49, 20);
            this.textBoxMaximumWarning.TabIndex = 4;
            // 
            // textBoxMinimumWarning
            // 
            this.textBoxMinimumWarning.Location = new System.Drawing.Point(183, 211);
            this.textBoxMinimumWarning.Name = "textBoxMinimumWarning";
            this.textBoxMinimumWarning.Size = new System.Drawing.Size(49, 20);
            this.textBoxMinimumWarning.TabIndex = 5;
            // 
            // VariableData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(366, 352);
            this.Controls.Add(this.textBoxMinimumWarning);
            this.Controls.Add(this.textBoxMaximumWarning);
            this.Controls.Add(this.label_MinimumWarningValue);
            this.Controls.Add(this.label_MaximumWarningValue);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.textBoxValue);
            this.Controls.Add(this.textBoxMinimum);
            this.Controls.Add(this.textBoxMaximum);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.labelActual);
            this.Controls.Add(this.labelMinimum);
            this.Controls.Add(this.labelMaximum);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.labelTitle);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "VariableData";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "New Variable";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelMaximum;
        private System.Windows.Forms.Label labelMinimum;
        private System.Windows.Forms.Label labelActual;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.TextBox textBoxMaximum;
        private System.Windows.Forms.TextBox textBoxMinimum;
        private System.Windows.Forms.TextBox textBoxValue;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label label_MaximumWarningValue;
        private System.Windows.Forms.Label label_MinimumWarningValue;
        private System.Windows.Forms.TextBox textBoxMaximumWarning;
        private System.Windows.Forms.TextBox textBoxMinimumWarning;
    }
}