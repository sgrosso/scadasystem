﻿using System;
using System.Windows.Forms;
using Domain;

namespace UI
{
    public partial class DeviceView : Form
    {
        private Device deviceToModify;
        public DeviceView(Device device)
        {
            InitializeComponent();
            deviceToModify = device;
            LoadData();
        }

        private void LoadData()
        {
            comboBoxDeviceType.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxDeviceType.Items.AddRange(ProductionPlant.GetInstance().ListDeviceTypes().ToArray());
            if (!deviceToModify.IsEmpty())
            {
                labelTitle.Text = "Modify Device";
                textBoxName.Text = deviceToModify.Name;
                comboBoxDeviceType.SelectedItem = deviceToModify.Type;
                checkBoxInuse.Enabled = true;
                checkBoxInuse.Checked = deviceToModify.InUse;
            }
            else
            {
                labelTitle.Text = "New Device";
                checkBoxInuse.Enabled = false;
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            int MAINTENANCE_TYPE = 3;
            Maintenance newMaintenanceWindow = new Maintenance(MAINTENANCE_TYPE);
            this.Dispose();
            newMaintenanceWindow.Show();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            if (deviceToModify.IsEmpty())
            {
                AddDevice();
            }
            else
            {
                ModifyDevice();
            }
        }
        private void AddDevice()
        {
            if (ThereAreEmptyComponents())
            {
                MessageBox.Show("You must fill all the fields", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                Domain.Device newDevice = CreateNewDevice();
                bool wasCorrectlyAdded = ProductionPlant.GetInstance().AddDevice(newDevice);
                if (wasCorrectlyAdded)
                {
                    MessageBox.Show("The device was correctly added", "Success", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    this.Dispose();
                    int MAINTENANCE_TYPE = 3;
                    Maintenance newMaintenanceWindow = new Maintenance(MAINTENANCE_TYPE);
                    newMaintenanceWindow.Show();
                }
                else
                {
                    MessageBox.Show("The device was already added", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void ModifyDevice()
        {
            if (ThereAreEmptyComponents())
            {
                MessageBox.Show("You must fill all the fields", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                deviceToModify.Name = textBoxName.Text.Trim();
                deviceToModify.Type = comboBoxDeviceType.SelectedItem as DeviceType;
                deviceToModify.InUse = checkBoxInuse.Checked;
                bool wasCorrectlyModified = ProductionPlant.GetInstance().ModifyDevice(deviceToModify);
                if (wasCorrectlyModified)
                {
                    MessageBox.Show("The device was correctly modified", "Success", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    this.Dispose();
                    int MAINTENANCE_TYPE = 3;
                    Maintenance newMaintenanceWindow = new Maintenance(MAINTENANCE_TYPE);
                    newMaintenanceWindow.Show();
                }
                else
                {
                    MessageBox.Show("The device wasn't modified", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private bool ThereAreEmptyComponents()
        {
            String name = textBoxName.Text;
            Object selectedItem = comboBoxDeviceType.SelectedItem;
            return String.IsNullOrEmpty(name) || selectedItem==null;
        }

        private Device CreateNewDevice()
        {
            Object selectedItem = comboBoxDeviceType.SelectedItem;
            Device newDevice = new Device()
            {
                Name = textBoxName.Text.Trim(),
                Type = selectedItem as DeviceType,
                InUse = false,
                OutOfRangeVariablesNumber = 0
            };
            return newDevice;
        }

        private void buttonNewDeviceType_Click(object sender, EventArgs e)
        {
            DeviceType newDeviceType = new DeviceType();
            DeviceTypeView newDeviceTypeWindow = new DeviceTypeView(newDeviceType);
            newDeviceTypeWindow.ShowDialog();
            comboBoxDeviceType.DataSource = ProductionPlant.GetInstance().ListDeviceTypes().ToArray();
        }

        private void buttonModify_Click(object sender, EventArgs e)
        {
            Object selectedItem = comboBoxDeviceType.SelectedItem;
            DeviceType typetoModify = selectedItem as DeviceType;
            DeviceTypeView newDeviceTypeViewWindow = new DeviceTypeView(typetoModify);
            newDeviceTypeViewWindow.ShowDialog();
            comboBoxDeviceType.DataSource = ProductionPlant.GetInstance().ListDeviceTypes().ToArray();
        }
    }
}
