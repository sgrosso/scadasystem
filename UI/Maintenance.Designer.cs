﻿namespace UI
{
    partial class Maintenance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TitleLabel = new System.Windows.Forms.Label();
            this.NewButton = new System.Windows.Forms.Button();
            this.ModifyButton = new System.Windows.Forms.Button();
            this.DeleteButton = new System.Windows.Forms.Button();
            this.VariablesButton = new System.Windows.Forms.Button();
            this.BackButton = new System.Windows.Forms.Button();
            this.listBoxPlantComponent = new System.Windows.Forms.ListBox();
            this.Button_ReportIncident = new System.Windows.Forms.Button();
            this.Button_ViewIncidents = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // TitleLabel
            // 
            this.TitleLabel.AutoSize = true;
            this.TitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TitleLabel.Location = new System.Drawing.Point(214, 9);
            this.TitleLabel.Name = "TitleLabel";
            this.TitleLabel.Size = new System.Drawing.Size(169, 31);
            this.TitleLabel.TabIndex = 1;
            this.TitleLabel.Text = "Maintenance";
            // 
            // NewButton
            // 
            this.NewButton.Location = new System.Drawing.Point(522, 73);
            this.NewButton.Name = "NewButton";
            this.NewButton.Size = new System.Drawing.Size(88, 23);
            this.NewButton.TabIndex = 2;
            this.NewButton.Text = "New";
            this.NewButton.UseVisualStyleBackColor = true;
            this.NewButton.Click += new System.EventHandler(this.NewButton_Click);
            // 
            // ModifyButton
            // 
            this.ModifyButton.Location = new System.Drawing.Point(522, 115);
            this.ModifyButton.Name = "ModifyButton";
            this.ModifyButton.Size = new System.Drawing.Size(88, 23);
            this.ModifyButton.TabIndex = 3;
            this.ModifyButton.Text = "Modify";
            this.ModifyButton.UseVisualStyleBackColor = true;
            this.ModifyButton.Click += new System.EventHandler(this.ModifyButton_Click);
            // 
            // DeleteButton
            // 
            this.DeleteButton.Location = new System.Drawing.Point(522, 156);
            this.DeleteButton.Name = "DeleteButton";
            this.DeleteButton.Size = new System.Drawing.Size(88, 23);
            this.DeleteButton.TabIndex = 4;
            this.DeleteButton.Text = "Delete";
            this.DeleteButton.UseVisualStyleBackColor = true;
            this.DeleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // VariablesButton
            // 
            this.VariablesButton.Location = new System.Drawing.Point(522, 199);
            this.VariablesButton.Name = "VariablesButton";
            this.VariablesButton.Size = new System.Drawing.Size(88, 23);
            this.VariablesButton.TabIndex = 5;
            this.VariablesButton.Text = "Variables";
            this.VariablesButton.UseVisualStyleBackColor = true;
            this.VariablesButton.Click += new System.EventHandler(this.VariablesButton_Click);
            // 
            // BackButton
            // 
            this.BackButton.Location = new System.Drawing.Point(522, 325);
            this.BackButton.Name = "BackButton";
            this.BackButton.Size = new System.Drawing.Size(88, 23);
            this.BackButton.TabIndex = 6;
            this.BackButton.Text = "Back";
            this.BackButton.UseVisualStyleBackColor = true;
            this.BackButton.Click += new System.EventHandler(this.BackButton_Click);
            // 
            // listBoxPlantComponent
            // 
            this.listBoxPlantComponent.FormattingEnabled = true;
            this.listBoxPlantComponent.Location = new System.Drawing.Point(12, 73);
            this.listBoxPlantComponent.Name = "listBoxPlantComponent";
            this.listBoxPlantComponent.Size = new System.Drawing.Size(504, 225);
            this.listBoxPlantComponent.TabIndex = 7;
            // 
            // Button_ReportIncident
            // 
            this.Button_ReportIncident.Location = new System.Drawing.Point(522, 238);
            this.Button_ReportIncident.Name = "Button_ReportIncident";
            this.Button_ReportIncident.Size = new System.Drawing.Size(88, 23);
            this.Button_ReportIncident.TabIndex = 8;
            this.Button_ReportIncident.Text = "New Incident";
            this.Button_ReportIncident.UseVisualStyleBackColor = true;
            this.Button_ReportIncident.Click += new System.EventHandler(this.Button_ReportIncident_Click);
            // 
            // Button_ViewIncidents
            // 
            this.Button_ViewIncidents.Location = new System.Drawing.Point(522, 275);
            this.Button_ViewIncidents.Name = "Button_ViewIncidents";
            this.Button_ViewIncidents.Size = new System.Drawing.Size(88, 23);
            this.Button_ViewIncidents.TabIndex = 9;
            this.Button_ViewIncidents.Text = "View Incidents";
            this.Button_ViewIncidents.UseVisualStyleBackColor = true;
            this.Button_ViewIncidents.Click += new System.EventHandler(this.Button_ViewIncidents_Click);
            // 
            // Maintenance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(618, 356);
            this.Controls.Add(this.Button_ViewIncidents);
            this.Controls.Add(this.Button_ReportIncident);
            this.Controls.Add(this.listBoxPlantComponent);
            this.Controls.Add(this.BackButton);
            this.Controls.Add(this.VariablesButton);
            this.Controls.Add(this.DeleteButton);
            this.Controls.Add(this.ModifyButton);
            this.Controls.Add(this.NewButton);
            this.Controls.Add(this.TitleLabel);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Maintenance";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Maintenance";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label TitleLabel;
        private System.Windows.Forms.Button NewButton;
        private System.Windows.Forms.Button ModifyButton;
        private System.Windows.Forms.Button DeleteButton;
        private System.Windows.Forms.Button VariablesButton;
        private System.Windows.Forms.Button BackButton;
        private System.Windows.Forms.ListBox listBoxPlantComponent;
        private System.Windows.Forms.Button Button_ReportIncident;
        private System.Windows.Forms.Button Button_ViewIncidents;
    }
}