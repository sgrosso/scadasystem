﻿namespace UI
{
    partial class DisplayIncidentsReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBox_FilteredIncidents = new System.Windows.Forms.ListBox();
            this.groupBox_ListContaniner = new System.Windows.Forms.GroupBox();
            this.label_SearchDetails = new System.Windows.Forms.Label();
            this.groupBox_ListContaniner.SuspendLayout();
            this.SuspendLayout();
            // 
            // listBox_FilteredIncidents
            // 
            this.listBox_FilteredIncidents.FormattingEnabled = true;
            this.listBox_FilteredIncidents.HorizontalScrollbar = true;
            this.listBox_FilteredIncidents.Location = new System.Drawing.Point(6, 15);
            this.listBox_FilteredIncidents.Name = "listBox_FilteredIncidents";
            this.listBox_FilteredIncidents.Size = new System.Drawing.Size(605, 433);
            this.listBox_FilteredIncidents.TabIndex = 0;
            // 
            // groupBox_ListContaniner
            // 
            this.groupBox_ListContaniner.Controls.Add(this.listBox_FilteredIncidents);
            this.groupBox_ListContaniner.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox_ListContaniner.Location = new System.Drawing.Point(12, 48);
            this.groupBox_ListContaniner.Name = "groupBox_ListContaniner";
            this.groupBox_ListContaniner.Size = new System.Drawing.Size(617, 459);
            this.groupBox_ListContaniner.TabIndex = 1;
            this.groupBox_ListContaniner.TabStop = false;
            this.groupBox_ListContaniner.Text = "Registered incident are listed:";
            // 
            // label_SearchDetails
            // 
            this.label_SearchDetails.AutoSize = true;
            this.label_SearchDetails.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_SearchDetails.Location = new System.Drawing.Point(15, 15);
            this.label_SearchDetails.Name = "label_SearchDetails";
            this.label_SearchDetails.Size = new System.Drawing.Size(112, 17);
            this.label_SearchDetails.TabIndex = 2;
            this.label_SearchDetails.Text = "Search details";
            // 
            // DisplayIncidentsReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(641, 515);
            this.Controls.Add(this.label_SearchDetails);
            this.Controls.Add(this.groupBox_ListContaniner);
            this.Name = "DisplayIncidentsReport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "View reported incidents";
            this.groupBox_ListContaniner.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBox_FilteredIncidents;
        private System.Windows.Forms.GroupBox groupBox_ListContaniner;
        private System.Windows.Forms.Label label_SearchDetails;
    }
}