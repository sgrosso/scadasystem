﻿using Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI
{
    public partial class DeviceTypeView : Form
    {
        DeviceType typeToModify;
        public DeviceTypeView(DeviceType type)
        {
            InitializeComponent();
            typeToModify = type;
            LoadData();
        }

        private void LoadData()
        {
            if (typeToModify.IsEmpty())
            {
                labelTitle.Text = "New Device Type";
            }
            else
            {
                labelTitle.Text = "Modify Device Type";
                textBoxName.Text = typeToModify.Name;
                richTextBoxDescription.Text = typeToModify.Description;
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            if (typeToModify.IsEmpty())
            {
                AddDeviceType();
            }
            else
            {
                ModifyDeviceType();
            }
        }

        private void AddDeviceType()
        {
            if (ThereAreEmptyComponents())
            {
                MessageBox.Show("You must fill all the fields", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                DeviceType newDeviceType = CreateNewDeviceType();
                bool wasCorrectlyAdded = ProductionPlant.GetInstance().AddDeviceType(newDeviceType);
                if (wasCorrectlyAdded)
                {
                    MessageBox.Show("The device type was correctly added", "Success", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    this.Dispose();
                }
                else
                {
                    MessageBox.Show("The device type was already added", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void ModifyDeviceType()
        {
            if (ThereAreEmptyComponents())
            {
                MessageBox.Show("You must fill all the fields", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                typeToModify.Name = textBoxName.Text.Trim();
                typeToModify.Description = richTextBoxDescription.Text.Trim();
                bool wasCorrectlyModified = ProductionPlant.GetInstance().ModifyDeviceType(typeToModify);
                if (wasCorrectlyModified)
                {
                    MessageBox.Show("The device type was correctly modified", "Success", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    this.Dispose();
                }
                else
                {
                    MessageBox.Show("The device type wasn't modified", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private bool ThereAreEmptyComponents()
        {
            String name = textBoxName.Text;
            String description = richTextBoxDescription.Text;
            return String.IsNullOrEmpty(name) || String.IsNullOrEmpty(description);
        }

        private DeviceType CreateNewDeviceType()
        {
            DeviceType newDeviceType = new DeviceType()
            {
                Name = textBoxName.Text,
                Description= richTextBoxDescription.Text
            };
            return newDeviceType;
        }
    }
}
