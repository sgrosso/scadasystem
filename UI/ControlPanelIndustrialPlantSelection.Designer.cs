﻿namespace UI
{
    partial class ControlPanelIndustrialPlantSelection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBox_IndustrialPlants = new System.Windows.Forms.ListBox();
            this.panel_IndustrialPlantsListboxContainer = new System.Windows.Forms.Panel();
            this.label_WindowTitle = new System.Windows.Forms.Label();
            this.Button_DisplayFirstLevelInstallations = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Button_Cancel = new System.Windows.Forms.Button();
            this.panel_IndustrialPlantsListboxContainer.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // listBox_IndustrialPlants
            // 
            this.listBox_IndustrialPlants.FormattingEnabled = true;
            this.listBox_IndustrialPlants.Location = new System.Drawing.Point(12, 13);
            this.listBox_IndustrialPlants.Name = "listBox_IndustrialPlants";
            this.listBox_IndustrialPlants.Size = new System.Drawing.Size(512, 368);
            this.listBox_IndustrialPlants.TabIndex = 0;
            // 
            // panel_IndustrialPlantsListboxContainer
            // 
            this.panel_IndustrialPlantsListboxContainer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel_IndustrialPlantsListboxContainer.Controls.Add(this.listBox_IndustrialPlants);
            this.panel_IndustrialPlantsListboxContainer.Location = new System.Drawing.Point(12, 33);
            this.panel_IndustrialPlantsListboxContainer.Name = "panel_IndustrialPlantsListboxContainer";
            this.panel_IndustrialPlantsListboxContainer.Size = new System.Drawing.Size(531, 398);
            this.panel_IndustrialPlantsListboxContainer.TabIndex = 1;
            // 
            // label_WindowTitle
            // 
            this.label_WindowTitle.AutoSize = true;
            this.label_WindowTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_WindowTitle.Location = new System.Drawing.Point(22, 9);
            this.label_WindowTitle.Name = "label_WindowTitle";
            this.label_WindowTitle.Size = new System.Drawing.Size(226, 20);
            this.label_WindowTitle.TabIndex = 2;
            this.label_WindowTitle.Text = "All industrial plantas are shown";
            // 
            // Button_DisplayFirstLevelInstallations
            // 
            this.Button_DisplayFirstLevelInstallations.Location = new System.Drawing.Point(3, 13);
            this.Button_DisplayFirstLevelInstallations.Name = "Button_DisplayFirstLevelInstallations";
            this.Button_DisplayFirstLevelInstallations.Size = new System.Drawing.Size(119, 38);
            this.Button_DisplayFirstLevelInstallations.TabIndex = 3;
            this.Button_DisplayFirstLevelInstallations.Text = "Display first level installations";
            this.Button_DisplayFirstLevelInstallations.UseVisualStyleBackColor = true;
            this.Button_DisplayFirstLevelInstallations.Click += new System.EventHandler(this.Button_DisplayFirstLevelInstallations_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.Button_Cancel);
            this.panel1.Controls.Add(this.Button_DisplayFirstLevelInstallations);
            this.panel1.Location = new System.Drawing.Point(549, 33);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(130, 398);
            this.panel1.TabIndex = 4;
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.Location = new System.Drawing.Point(3, 67);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(119, 38);
            this.Button_Cancel.TabIndex = 4;
            this.Button_Cancel.Text = "Cancel";
            this.Button_Cancel.UseVisualStyleBackColor = true;
            // 
            // ControlPanelIndustrialPlantSelection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(691, 439);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label_WindowTitle);
            this.Controls.Add(this.panel_IndustrialPlantsListboxContainer);
            this.Name = "ControlPanelIndustrialPlantSelection";
            this.Text = "Industrial plants selection";
            this.Load += new System.EventHandler(this.ControlPanelIndustrialPlantSelection_Load);
            this.panel_IndustrialPlantsListboxContainer.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBox_IndustrialPlants;
        private System.Windows.Forms.Panel panel_IndustrialPlantsListboxContainer;
        private System.Windows.Forms.Label label_WindowTitle;
        private System.Windows.Forms.Button Button_DisplayFirstLevelInstallations;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button Button_Cancel;
    }
}