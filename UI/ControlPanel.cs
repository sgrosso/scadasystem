﻿using Domain;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace UI
{
    public partial class ControlPanel : Form, Observer
    {
        private IndustrialPlant industrialPlant;
        private int NUMBER_OF_CONTROLS_PER_ROW = 5;
        public ControlPanel(IndustrialPlant industrialPlant)
        {
            InitializeComponent();
            this.industrialPlant = industrialPlant;
            LoadAlarms();
        }

        private void LoadAlarms()
        {
            List<Installation> firstLevelInstallations = GetFirstLevelComponents(industrialPlant);
            if (firstLevelInstallations.Count > 0)
            {
                label_WindowMessage.Text = "First level Installations";
                label_WindowMessage.ForeColor = Color.Black;
                SubscribeToComponentsUpdates(firstLevelInstallations);
                int alarmViewerWidth = GetAlarmViewerControlWidth();
                Point controlPosition = new Point(0, 30);
                int controlsAddedPerRow = 0;
                foreach(PlantComponent plantComponent in firstLevelInstallations)
                {
                    AlarmViewer alarmViewerControl = new AlarmViewer(plantComponent);
                    alarmViewerControl.Width = alarmViewerWidth;
                    alarmViewerControl.Name = "ComponentControl_" + plantComponent.Identifier;
                    if(controlsAddedPerRow < NUMBER_OF_CONTROLS_PER_ROW)
                    {
                        AddAlarmViewerToForm(alarmViewerControl, controlPosition);
                        controlsAddedPerRow++;
                        controlPosition = MoveControlToTheRight(controlPosition, alarmViewerWidth);
                    }
                    else
                    {
                        controlPosition = MoveControlToTheNextRow(controlPosition, alarmViewerControl.Height);
                        AddAlarmViewerToForm(alarmViewerControl, controlPosition);
                        controlsAddedPerRow = 1;
                        controlPosition = MoveControlToTheRight(controlPosition, alarmViewerWidth);
                    }
                }
            }
            else
            {
                label_WindowMessage.ForeColor = Color.Red;
                label_WindowMessage.Text = "No first level installations found";
            }
        }


        private void AddAlarmViewerToForm(AlarmViewer alarmViewer, Point position)
        {
            alarmViewer.Location = position;
            this.Controls.Add(alarmViewer);
        }

        private Point MoveControlToTheRight(Point actualPosition, int moveValue)
        {
            actualPosition.X += moveValue;
            return actualPosition;
        }

        private Point MoveControlToTheNextRow(Point actualPosition, int moveValue)
        {
            actualPosition.X = 0;
            actualPosition.Y += moveValue;
            return actualPosition;
        }
        private List<Installation> GetFirstLevelComponents(IndustrialPlant industrialPlant)
        {
            return ProductionPlant.GetInstance().ListIndustrialPlantFirstLevelInstallations(industrialPlant);   
        }

        private int GetAlarmViewerControlWidth()
        {
            double controlWidth = (Width - 10) / NUMBER_OF_CONTROLS_PER_ROW;
            return (int) Math.Truncate(controlWidth);
        }

        private void SubscribeToComponentsUpdates(List<Installation> firstLevelComponents)
        {
            foreach(var installation in firstLevelComponents)
            {
                installation.AddObserver(this);
            }
        }

        public void Update(Observable observable)
        {
            PlantComponent plantComponent = observable as PlantComponent;
            AlarmViewer plantComponentControl = (AlarmViewer)(Controls.Find("ComponentControl_" + plantComponent.Identifier, true))[0];
            plantComponentControl.UpdateControl(plantComponent);
        }

        private void ControlPanel_FormClosing(object sender, FormClosingEventArgs e)
        {
            DeleteControlPanelFromObserversList();
        }

        private void DeleteControlPanelFromObserversList()
        {
            List<Installation> installations = ProductionPlant.GetInstance().ListInstallations();
            foreach(var installation in installations)
            {
                installation.RemoveObserver(this);
            }
        }

        private void Button_Refresh_Click(object sender, EventArgs e)
        {
            DeleteControlPanelFromObserversList();
            LoadAlarms();
        }
    }
}
