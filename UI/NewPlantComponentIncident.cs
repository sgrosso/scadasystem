﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Domain;

namespace UI
{
    public partial class NewPlantComponentIncident : Form
    {
        private PlantComponent plantComponent;
        public NewPlantComponentIncident(PlantComponent aPlantComponent)
        {
            plantComponent = aPlantComponent;
            InitializeComponent();
            SetPlantComponentData();

        }

        private void SetPlantComponentData()
        {
            label_PlantComponentIdentifier.Text = plantComponent.Name;
            comboBox_IncidentSeverity.SelectedIndex = 0;
        }

        private void Button_Ok_Click(object sender, EventArgs e)
        {
            Incident incident = new Incident();
            incident.Date = dateTimePicker_IncidentDate.Value;
            incident.Severity = int.Parse(comboBox_IncidentSeverity.SelectedItem.ToString());
            incident.Description = richTextBox_IncidentDescription.Text;
            plantComponent.ReportIncident(incident);
            this.Dispose();
        }

        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
