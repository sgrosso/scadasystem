﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Domain;

namespace UI
{
    public partial class ControlPanelIndustrialPlantSelection : Form
    {
        public ControlPanelIndustrialPlantSelection()
        {
            InitializeComponent();
            LoadIndustrialPlantsListBox();
        }

        private void ControlPanelIndustrialPlantSelection_Load(object sender, EventArgs e)
        {

        }

        private void LoadIndustrialPlantsListBox()
        {
            List <IndustrialPlant> industrialPlantsList = ProductionPlant.GetInstance().ListIndustrialPlants();
            listBox_IndustrialPlants.Items.AddRange(industrialPlantsList.ToArray());
        }

        private void Button_DisplayFirstLevelInstallations_Click(object sender, EventArgs e)
        {
            if (listBoxItemIsSelected())
            {
                IndustrialPlant selectedPlant = listBox_IndustrialPlants.SelectedItem as IndustrialPlant;
                ControlPanel controlPanel = new ControlPanel(selectedPlant);
                controlPanel.Show();
                this.Dispose();
            }
            
        }

        private bool listBoxItemIsSelected()
        {
            int NO_SELECTION_INDEX = -1;
            if (listBox_IndustrialPlants.SelectedIndex == NO_SELECTION_INDEX)
            {
                return false;
            }
            return true;
        }
    }
}
