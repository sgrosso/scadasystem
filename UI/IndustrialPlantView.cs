﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Domain;

namespace UI
{
    public partial class IndustrialPlantView : Form
    {
        IndustrialPlant industrialPlantToModify;
        public IndustrialPlantView(IndustrialPlant newIndustrialPlant)
        {
            InitializeComponent();
            industrialPlantToModify = newIndustrialPlant;
            LoadData();
        }

        public void LoadData()
        {
            listBoxChildren.Items.Clear();
            listBoxCandidates.Items.Clear();
            listBoxCandidates.Items.AddRange(CandidatesChildren().ToArray());
            listBoxChildren.Items.AddRange(industrialPlantToModify.ChildrenComponents.ToArray());
            if (industrialPlantToModify.IsEmpty())
            {
                labelTitle.Text = "New Industrial Plant";
            }
            else
            {
                labelTitle.Text = "Modify Industrial Plant";
                textBoxName.Text = industrialPlantToModify.Name;
                textBoxCountry.Text = industrialPlantToModify.Country;
                textBoxCity.Text = industrialPlantToModify.City;
                textBoxAddress.Text = industrialPlantToModify.Address;
            }
        }

        private List<PlantComponent> CandidatesChildren()
        {
            List<PlantComponent> candidatesChildren = new List<PlantComponent>();
            foreach (object component in ProductionPlant.GetInstance().ListPlantComponents())
            {
                PlantComponent plantComponent = component as PlantComponent;
                if (!plantComponent.IsUsed() && industrialPlantToModify.Identifier != plantComponent.Identifier)
                {
                    candidatesChildren.Add(plantComponent);
                }
            }
            return candidatesChildren;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            int MAINTENANCE_TYPE = 1;
            this.Dispose();
            Maintenance newMaintenanceWindow = new Maintenance(MAINTENANCE_TYPE);
            newMaintenanceWindow.Show();
        }

        private void buttonToChildren_Click(object sender, EventArgs e)
        {
            int NO_SELECTION_INDEX = -1;
            if (listBoxCandidates.SelectedIndex != NO_SELECTION_INDEX)
            {
                Object selectedItem = listBoxCandidates.SelectedItem;
                listBoxChildren.Items.Add(selectedItem);
                listBoxCandidates.Items.Remove(selectedItem);
            }
            else
            {
                MessageBox.Show("You must select an item", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void buttonDeleteChildren_Click(object sender, EventArgs e)
        {
            int NO_SELECTION_INDEX = -1;
            if (listBoxChildren.SelectedIndex != NO_SELECTION_INDEX)
            {
                Object selectedItem = listBoxChildren.SelectedItem;
                PlantComponent plantComponent = selectedItem as PlantComponent;
                listBoxCandidates.Items.Add(plantComponent);
                listBoxChildren.Items.Remove(plantComponent);
            }
            else
            {
                MessageBox.Show("You must select an item", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (industrialPlantToModify.IsEmpty())
            {
                NewIndustrialPlant();
            }
            else
            {
                ModifyIndustrialPlant();
            }
            CandidatesChildrenSetNotInUse();
        }

        private void NewIndustrialPlant()
        {
            String industrialPlantName = textBoxName.Text.Trim();
            String industrialPlantCountry = textBoxCountry.Text.Trim();
            String industrialPlantCity = textBoxCity.Text.Trim();
            String industrialPlantAddress = textBoxAddress.Text.Trim();
            if (String.IsNullOrEmpty(industrialPlantName) || String.IsNullOrEmpty(industrialPlantCountry) || String.IsNullOrEmpty(industrialPlantCity) || String.IsNullOrEmpty(industrialPlantAddress))
            {
                MessageBox.Show("You must fill all the fields", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                industrialPlantToModify.ChildrenComponents = null;
                industrialPlantToModify.ChildrenComponents = ChildrenComponentsFromListBox();
                industrialPlantToModify.Name = industrialPlantName;
                industrialPlantToModify.Country = industrialPlantCountry;
                industrialPlantToModify.City = industrialPlantCity;
                industrialPlantToModify.Address = industrialPlantAddress;
                bool wasCorrectlyAdded = ProductionPlant.GetInstance().AddIndustrialPlant(industrialPlantToModify);
                if (wasCorrectlyAdded)
                {
                    MessageBox.Show("Industrial Plant correctly added", "Success", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    this.Dispose();
                    int MAINTENANCE_TYPE = 1;
                    Maintenance newMaintenanceWindow = new Maintenance(MAINTENANCE_TYPE);
                    newMaintenanceWindow.Show();
                }
                else
                {
                    MessageBox.Show("The industrial plant was already added", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void ModifyIndustrialPlant()
        {
            String industrialPlantName = textBoxName.Text.Trim();
            String industrialPlantCountry = textBoxCountry.Text.Trim();
            String industrialPlantCity = textBoxCity.Text.Trim();
            String industrialPlantAddress = textBoxAddress.Text.Trim();
            if (String.IsNullOrEmpty(industrialPlantName) || String.IsNullOrEmpty(industrialPlantCountry) || String.IsNullOrEmpty(industrialPlantCity) || String.IsNullOrEmpty(industrialPlantAddress))
            {
                MessageBox.Show("You must fill all the fields", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                DeleteAllChildrenComponents(industrialPlantToModify);
                industrialPlantToModify.ChildrenComponents = null;
                industrialPlantToModify.ChildrenComponents = ChildrenComponentsFromListBox();
                industrialPlantToModify.Name = industrialPlantName;
                industrialPlantToModify.Country = industrialPlantCountry;
                industrialPlantToModify.City = industrialPlantCity;
                industrialPlantToModify.Address = industrialPlantAddress;
                bool wasCorrectlyModified = ProductionPlant.GetInstance().ModifyIndustrialPlant(industrialPlantToModify);
                if (wasCorrectlyModified)
                {
                    MessageBox.Show("The industrial plant was correctly modified", "Success", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    this.Dispose();
                    int MAINTENANCE_TYPE = 1;
                    Maintenance newMaintenanceWindow = new Maintenance(MAINTENANCE_TYPE);
                    newMaintenanceWindow.Show();
                }
                else
                {
                    MessageBox.Show("The industrial plant wasn't modified", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void CandidatesChildrenSetNotInUse()
        {
            foreach (object listItem in listBoxCandidates.Items)
            {
                PlantComponent component = listItem as PlantComponent;
                component.SetNotInUse();
            }
        }

        private List<PlantComponent> ChildrenComponentsFromListBox()
        {
            List<PlantComponent> newChildrenComponentList = new List<PlantComponent>();
            foreach (object listItem in listBoxChildren.Items)
            {
                PlantComponent component = listItem as PlantComponent;
                component.SetInUseBy(industrialPlantToModify);
                newChildrenComponentList.Add(component);
            }
            return newChildrenComponentList;
        }

        private void DeleteAllChildrenComponents(IndustrialPlant industrialPlant)
        {
            foreach (var aComponent in industrialPlantToModify.ChildrenComponents)
            {
                aComponent.Parent = null;
            }
        }
    }
}
