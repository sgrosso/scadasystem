﻿namespace UI
{
    partial class InstallationView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelTitle = new System.Windows.Forms.Label();
            this.labelName = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.listBoxComponents = new System.Windows.Forms.ListBox();
            this.listBoxChildren = new System.Windows.Forms.ListBox();
            this.buttonToChidren = new System.Windows.Forms.Button();
            this.buttonDeleteChildren = new System.Windows.Forms.Button();
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.labelCandidates = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitle.Location = new System.Drawing.Point(118, 9);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(232, 31);
            this.labelTitle.TabIndex = 0;
            this.labelTitle.Text = "Installation Modify";
            this.labelTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(74, 73);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(38, 13);
            this.labelName.TabIndex = 1;
            this.labelName.Text = "Name:";
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(153, 70);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(197, 20);
            this.textBoxName.TabIndex = 2;
            // 
            // listBoxComponents
            // 
            this.listBoxComponents.FormattingEnabled = true;
            this.listBoxComponents.Location = new System.Drawing.Point(42, 122);
            this.listBoxComponents.Name = "listBoxComponents";
            this.listBoxComponents.Size = new System.Drawing.Size(161, 212);
            this.listBoxComponents.TabIndex = 3;
            // 
            // listBoxChildren
            // 
            this.listBoxChildren.FormattingEnabled = true;
            this.listBoxChildren.Location = new System.Drawing.Point(270, 122);
            this.listBoxChildren.Name = "listBoxChildren";
            this.listBoxChildren.Size = new System.Drawing.Size(161, 212);
            this.listBoxChildren.TabIndex = 4;
            // 
            // buttonToChidren
            // 
            this.buttonToChidren.Location = new System.Drawing.Point(209, 184);
            this.buttonToChidren.Name = "buttonToChidren";
            this.buttonToChidren.Size = new System.Drawing.Size(48, 23);
            this.buttonToChidren.TabIndex = 5;
            this.buttonToChidren.Text = ">>";
            this.buttonToChidren.UseVisualStyleBackColor = true;
            this.buttonToChidren.Click += new System.EventHandler(this.buttonToChidren_Click);
            // 
            // buttonDeleteChildren
            // 
            this.buttonDeleteChildren.Location = new System.Drawing.Point(209, 229);
            this.buttonDeleteChildren.Name = "buttonDeleteChildren";
            this.buttonDeleteChildren.Size = new System.Drawing.Size(48, 23);
            this.buttonDeleteChildren.TabIndex = 6;
            this.buttonDeleteChildren.Text = "<<";
            this.buttonDeleteChildren.UseVisualStyleBackColor = true;
            this.buttonDeleteChildren.Click += new System.EventHandler(this.buttonDeleteChildren_Click);
            // 
            // buttonOk
            // 
            this.buttonOk.Location = new System.Drawing.Point(115, 357);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(75, 23);
            this.buttonOk.TabIndex = 7;
            this.buttonOk.Text = "OK";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(285, 357);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 8;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // labelCandidates
            // 
            this.labelCandidates.AutoSize = true;
            this.labelCandidates.Location = new System.Drawing.Point(39, 106);
            this.labelCandidates.Name = "labelCandidates";
            this.labelCandidates.Size = new System.Drawing.Size(101, 13);
            this.labelCandidates.TabIndex = 9;
            this.labelCandidates.Text = "Children Candidates";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(267, 106);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Children Components";
            // 
            // InstallationView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(467, 392);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelCandidates);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.buttonDeleteChildren);
            this.Controls.Add(this.buttonToChidren);
            this.Controls.Add(this.listBoxChildren);
            this.Controls.Add(this.listBoxComponents);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.labelTitle);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "InstallationView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Installation";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.ListBox listBoxComponents;
        private System.Windows.Forms.ListBox listBoxChildren;
        private System.Windows.Forms.Button buttonToChidren;
        private System.Windows.Forms.Button buttonDeleteChildren;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label labelCandidates;
        private System.Windows.Forms.Label label1;
    }
}