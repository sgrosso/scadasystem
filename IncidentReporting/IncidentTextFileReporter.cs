﻿using System;
using Domain;
using System.IO;
using System.Text;
using System.Configuration;

namespace IncidentReporting
{
    public class IncidentTextFileReporter : IncidentReporter
    {
        public void Report(Incident incident)
        {
            try
            {
                WriteIncidentInFile(incident);
            }
            catch(UnauthorizedAccessException exception)
            {
                throw new Exception("You don't have permissions to write the specified directory", exception);
            }
            catch(FileNotFoundException exception)
            {
                throw new Exception("The specified file couldn't be found", exception);
            }
            catch (Exception exception)
            {
                throw new Exception("The incident couldn't be saved", exception);
            }
        }

        public void WriteIncidentInFile(Incident incident)
        {
            String path = ConfigurationManager.AppSettings["TxtPath"];
            String incidentLine = BuildIncidentLine(incident);
            TextWriter textWriter = new StreamWriter(path, true);
            textWriter.WriteLine(incidentLine);
            textWriter.Close();
        }

        private String BuildIncidentLine(Incident incident)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(incident.Date.ToString());
            stringBuilder.Append(";");
            stringBuilder.Append(incident.Severity.ToString());
            stringBuilder.Append(";");
            stringBuilder.Append(incident.Description.ToString());
            stringBuilder.Append(";");
            stringBuilder.Append(incident.PlantComponentName.ToString());
            stringBuilder.Append(";");
            stringBuilder.Append(incident.PlantComponentIdentifier);
            return stringBuilder.ToString();
        }
    }
}
