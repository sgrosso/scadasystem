﻿using Domain;

namespace IncidentReporting
{
    public class IncidentDatabaseReporter : IncidentReporter
    {
        public void Report(Incident incident)
        {
            using (var incidentDatabaseContext = new IncidentDatabaseContext())
            {
                incidentDatabaseContext.Incidents.Add(incident);
                incidentDatabaseContext.SaveChanges();
            }
        }
    }
}
