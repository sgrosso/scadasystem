﻿using Domain;
using System.Data.Entity;

namespace IncidentReporting
{
    public class IncidentDatabaseContext : DbContext
    {
        public DbSet<Incident> Incidents { get; set; }
    }
}
