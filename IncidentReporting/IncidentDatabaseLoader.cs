﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IncidentReporting
{
    public class IncidentDatabaseLoader : IncidentLoader
    {
        public List<Incident> LoadIncidentsFilteringBy(Tuple<DateTime, DateTime> dateRangeFilter, int severity, PlantComponent plantComponent, bool executeRecursiveSearch)
        {
            List<Incident> incidentsFiltered;
            if (executeRecursiveSearch)
            {
                incidentsFiltered = GetPlantComponentIncidentsRecursively(dateRangeFilter, severity, plantComponent);
            }
            else
            {
                incidentsFiltered = GetPlantComponentIncidentsOnly(dateRangeFilter, severity, plantComponent);
            }
            return incidentsFiltered;
        }

        private List<Incident> GetPlantComponentIncidentsOnly(Tuple<DateTime, DateTime> dateRangeFilter, int severity ,PlantComponent plantComponent)
        {
            DateTime dateFilterFrom = dateRangeFilter.Item1;
            DateTime dateFilterTo = dateRangeFilter.Item2;
            using (var incidentDatabaseContext = new IncidentDatabaseContext())
            {
                var incidentsFromDatabase = from incident in incidentDatabaseContext.Incidents
                                            where incident.PlantComponentIdentifier == plantComponent.Identifier &&
                                            incident.Date >= dateFilterFrom && incident.Date <= dateFilterTo
                                            select incident;
                List<Incident> filteredIncidentList = incidentsFromDatabase.ToList();

                if (severity != 0)
                {
                    filteredIncidentList = filteredIncidentList.FindAll(i => i.Severity == severity);
                }
                return filteredIncidentList;
            }
        }

        private List<Incident> GetPlantComponentIncidentsRecursively(Tuple<DateTime, DateTime> dateRangeFilter, int severity, PlantComponent plantComponent)
        {
            List<Incident> filteredIncidents = GetPlantComponentIncidentsOnly(dateRangeFilter, severity, plantComponent);
            if(!plantComponent.IsDevice())
            {
                List<PlantComponent> plantComponentChildrenComponents;
                if (plantComponent.IsInstallation())
                {
                    Installation installation = plantComponent as Installation;
                    plantComponentChildrenComponents = ProductionPlant.GetInstance().ListInstallationChidrenComponents(installation);
                }
                else
                {
                    IndustrialPlant industrialPlant = plantComponent as IndustrialPlant;
                    plantComponentChildrenComponents = ProductionPlant.GetInstance().ListIndustrialPlantChidrenComponents(industrialPlant);
                }
                 
                foreach(var childComponent in plantComponentChildrenComponents)
                {
                    return MergeIncidentsList(filteredIncidents, GetPlantComponentIncidentsRecursively(dateRangeFilter, severity, childComponent));
                }
            }
            return filteredIncidents;
        }

        private List<Incident> MergeIncidentsList(List<Incident> anIncidentList, List<Incident> anotherIncidentList)
        {
            foreach(var incident in anotherIncidentList)
            {
                anIncidentList.Add(incident);
            }
            return anIncidentList;
        }
    }
}
