﻿using Domain;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;

namespace IncidentReporting
{
    public class IncidentTextFileLoader : IncidentLoader
    {
        public List<Incident> LoadIncidentsFilteringBy(Tuple<DateTime, DateTime> dateRangeFilter, int severity, PlantComponent plantComponent, bool executeRecursiveSearch)
        {
            List<Incident> filteredByComponentIncidents = new List<Incident>();
            DateTime dateFilterFrom = dateRangeFilter.Item1;
            DateTime dateFilterTo = dateRangeFilter.Item2;

            if (executeRecursiveSearch)
            {
                GetPlantComponentIncidentsRecursively(plantComponent, filteredByComponentIncidents);
            }
            else
            {
                filteredByComponentIncidents = GetPlantComponentIncidentsOnly(plantComponent);
            }


            List<Incident> filteredIncidents = filteredByComponentIncidents.FindAll(i => i.Date.Date >= dateFilterFrom.Date && i.Date.Date <= dateFilterTo.Date);
            
            if(severity != 0)
            {
                filteredIncidents = filteredIncidents.FindAll(i => i.Severity == severity);
            }
            return filteredIncidents;
        }

        private void GetPlantComponentIncidentsRecursively(PlantComponent plantComponent, List<Incident> filteredIncidentsReturned)
        {
            if (plantComponent == null)
                return;

            List<String> incidentsInTextFile = GetIncidentsReportedInTextFile();
            foreach(var line in incidentsInTextFile)
            {
                if(line != null && line != String.Empty)
                {
                    int componentLineIdentifier = GetComponentIdentifierFromLine(line);
                    if(componentLineIdentifier == plantComponent.Identifier)
                    {
                        Incident foundComponentIncident = CreateIncidentFromLine(line);
                        filteredIncidentsReturned.Add(foundComponentIncident);
                    }
                }
            }

            if (!plantComponent.IsDevice())
            {
                List<PlantComponent> plantComponentChildren;
                if (plantComponent.IsInstallation())
                {
                    Installation installation = plantComponent as Installation;
                    plantComponentChildren = ProductionPlant.GetInstance().ListInstallationChidrenComponents(installation);
                }
                else
                {
                    IndustrialPlant industrialPlant = plantComponent as IndustrialPlant;
                    plantComponentChildren = ProductionPlant.GetInstance().ListIndustrialPlantChidrenComponents(industrialPlant);
                }
                foreach(PlantComponent child in plantComponentChildren)
                {
                    GetPlantComponentIncidentsRecursively(child, filteredIncidentsReturned);
                }
            }
        }

        private List<Incident> GetPlantComponentIncidentsOnly(PlantComponent plantComponent)
        {
            List<Incident> filteredIncidents = new List<Incident>();

            if (plantComponent == null)
                return filteredIncidents;

            List<String> incidentsInTextFile = GetIncidentsReportedInTextFile();
            foreach (var line in incidentsInTextFile)
            {
                if (line != null && line != String.Empty)
                {
                    int componentLineIdentifier = GetComponentIdentifierFromLine(line);
                    if (componentLineIdentifier == plantComponent.Identifier)
                    {
                        Incident foundComponentIncident = CreateIncidentFromLine(line);
                        filteredIncidents.Add(foundComponentIncident);
                    }
                }
            }

            return filteredIncidents;
        }

        private List<String> GetIncidentsReportedInTextFile()
        {
            String path = ConfigurationManager.AppSettings["TxtPath"];
            if (File.Exists(path))
            {
                String[] linesInTextFile = File.ReadAllLines(path);
                return linesInTextFile.ToList();
            }
            return new List<String>();
        }

       private int GetComponentIdentifierFromLine(String line)
        {
            int componentLineIdentifier = 0;
            String [] lineTokens = line.Split(';');
            bool couldParseIdentifier = int.TryParse(lineTokens[lineTokens.Length - 1], out componentLineIdentifier);
            if (!couldParseIdentifier)
            {
                throw new Exception("The incidents textfile couldn't be read correctly");
            }

            return componentLineIdentifier;
        }

        private Incident CreateIncidentFromLine(String line)
        {
            String[] lineTokens = line.Split(';');
            Incident incident = new Incident();
            incident.Date = DateTime.Parse(lineTokens[0]);
            incident.Severity = int.Parse(lineTokens[1]);
            incident.Description = lineTokens[2];
            incident.PlantComponentName = lineTokens[3];
            incident.PlantComponentIdentifier = int.Parse(lineTokens[4]);
            return incident;            
        }

    }
}
