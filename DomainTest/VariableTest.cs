﻿using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace DomainTest
{
    [TestClass]
    public class VariableTest
    {
        private Variable variable;

        [TestInitialize()]
        public void InitializeVariableTest()
        {
            variable = new Variable();
        }
        [TestMethod]
        public void TestNameNotNull()
        {
            Assert.IsNotNull(variable.Name);
        }

        [TestMethod]
        public void TestMinimumValueEqualsMinDouble()
        {
            double minDoubleValue = double.MinValue;
            Assert.AreEqual(variable.MinimumValue, minDoubleValue);
        }

        [TestMethod]
        public void TestMinimumWarningValueEqualsMinDouble()
        {
            double minDoubleValue = double.MinValue;
            Assert.AreEqual(variable.MinimumWarningValue, minDoubleValue);
        }

        [TestMethod]
        public void TestMaximumValueEqualsMaxDouble()
        {
            double maxDoubleValue = double.MaxValue;
            Assert.AreEqual(variable.MaximumValue, maxDoubleValue);
        }

        [TestMethod]
        public void TestMaximumWarningValueEqualsMaxDouble()
        {
            double maxDoubleValue = double.MaxValue;
            Assert.AreEqual(variable.MaximumWarningValue, maxDoubleValue);
        }

        [TestMethod]
        public void TestDefaultValueEqualsZero()
        {
            double defaultValue = 0;
            Assert.AreEqual(variable.ActualValue, defaultValue);
        }

        [TestMethod]
        public void TestSetMinimumValue()
        {
            LoadVariableData();
            double expectedValue = -5.8;
            double actualMinimumValue = variable.MinimumValue;
            Assert.AreEqual(actualMinimumValue, expectedValue);
        }

        [TestMethod]
        public void TestSetMaximumValue()
        {
            LoadVariableData();
            double expectedValue = 10.5;
            double actualMaximumValue = variable.MaximumValue;
            Assert.AreEqual(actualMaximumValue, expectedValue);
        }

        [TestMethod]
        public void TestSetVariableValue()
        {
            LoadVariableData();
            double expectedValue = 8.2;
            double actualValue = variable.ActualValue;
            Assert.AreEqual(actualValue, expectedValue);
        }

        [TestMethod]
        public void TestVariableHistoricalValues()
        {
            LoadVariableData();
            double expectedValue = 8.2;
            double newVariableValue = 15.3;
            variable.ActualValue = newVariableValue;
            double firstHistoricalItemValue = variable.HistoricalValues[0].Item1;
            Assert.AreEqual(firstHistoricalItemValue, expectedValue);
        }

        [TestMethod]
        public void TestVariableValueIsInRange()
        {
            LoadVariableData();
            bool isInRange = variable.IsVariableValueInRange();
            Assert.IsTrue(isInRange);
        }

        [TestMethod]
        public void TestVariableValueIsInWarningRange()
        {
            LoadVariableData();
            bool isInWarningRange = variable.isVariableValueInWarningRange();
            Assert.IsTrue(isInWarningRange);
        }

        [TestMethod]
        public void TestVariableValueIsOutOfRange()
        {
            LoadVariableData();
            variable.ActualValue = 15.8;
            bool IsInRange = variable.IsVariableValueInRange();
            Assert.IsFalse(IsInRange);
        }

        [TestMethod]
        public void TestVariableEquality()
        {
            LoadVariableData();
            Variable anotherVariable = new Variable();
            anotherVariable.Identifier = variable.Identifier;
            Assert.IsTrue(variable.Equals(anotherVariable));
        }

        [TestMethod]
        public void TestVariableToString()
        {
            LoadVariableData();
            String variableToString = variable.ToString();
            String toStringExpected = "Variable de prueba, actual value: 8,2";
            Assert.AreEqual(toStringExpected, variableToString);
                
        }

        private void LoadVariableData()
        {
            variable.Identifier = 1;
            variable.Name = "Variable de prueba"; 
            variable.MinimumValue = -5.8;
            variable.MaximumValue = 10.5;
            variable.MinimumWarningValue = -2.5;
            variable.MaximumWarningValue = 9.0;
            variable.ActualValue = 8.2;
        }

    }
}
