﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Domain;
using Moq;
using Persistence;

namespace DomainTest
{
    [TestClass]
    public class ProductionPlantTest
    {
        ProductionPlant productionPlant = ProductionPlant.GetInstance();
        Mock<DomainPersistence> persistenceClass = new Mock<DomainPersistence>();

        [TestInitialize()]
        public void InitializeProductionPlantTest()
        {
            productionPlant.Repository = persistenceClass.Object;
        }

        [TestMethod]
        public void TestAddPlantComponent()
        {
            PlantComponent plantComponent = new Installation();
            productionPlant.AddPlantComponent(plantComponent);
            persistenceClass.Verify(repository => repository.AddPlantComponent(It.IsAny<PlantComponent>()));
        }

        [TestMethod]
        public void TestAddDevice()
        {
            Device device = new Device();
            productionPlant.AddDevice(device);
            persistenceClass.Verify(repository => repository.AddDevice(It.IsAny<Device>()));
        }

        [TestMethod]
        public void TestAddInstallation()
        {
            Installation installation = new Installation();
            productionPlant.AddInstallation(installation);
            persistenceClass.Verify(repository => repository.AddInstallation(It.IsAny<Installation>()));
        }

        [TestMethod]
        public void TestAddDeviceType()
        {
            DeviceType type = new DeviceType();
            productionPlant.AddDeviceType(type);
            persistenceClass.Verify(repository => repository.AddDeviceType(It.IsAny<DeviceType>()));
        }

        [TestMethod]
        public void TestDeletePlantComponent()
        {
            PlantComponent plantComponent = new Installation();
            productionPlant.DeletePlantComponent(plantComponent);
            persistenceClass.Verify(repository => repository.DeletePlantComponent(It.IsAny<PlantComponent>()));
        }

        [TestMethod]
        public void TestDeleteDevice()
        {
            Device device = new Device();
            productionPlant.DeleteDevice(device);
            persistenceClass.Verify(repository => repository.DeleteDevice(It.IsAny<Device>()));
        }

        [TestMethod]
        public void TestDeleteInstallation()
        {
            Installation installation = new Installation();
            productionPlant.DeleteInstallation(installation);
            persistenceClass.Verify(repository => repository.DeleteInstallation(It.IsAny<Installation>()));
        }

        [TestMethod]
        public void TestModifyPlantComponent()
        {
            PlantComponent plantComponent = new Installation();
            productionPlant.ModifyPlantComponent(plantComponent);
            persistenceClass.Verify(repository => repository.ModifyPlantComponent(It.IsAny<PlantComponent>()));
        }

        [TestMethod]
        public void TestModifyDevice()
        {
            Device device = new Device();
            productionPlant.ModifyDevice(device);
            persistenceClass.Verify(repository => repository.ModifyDevice(It.IsAny<Device>()));
        }

        [TestMethod]
        public void TestModifyInstallation()
        {
            Installation installation = new Installation();
            productionPlant.ModifyInstallation(installation);
            persistenceClass.Verify(repository => repository.ModifyInstallation(It.IsAny<Installation>()));
        }

        [TestMethod]
        public void TestModifyDeviceType()
        {
            DeviceType type = new DeviceType();
            productionPlant.ModifyDeviceType(type);
            persistenceClass.Verify(repository => repository.ModifyDeviceType(It.IsAny<DeviceType>()));
        }

        [TestMethod]
        public void TestListPlantComponents()
        {
            productionPlant.ListPlantComponents();
            persistenceClass.Verify(repository => repository.ListPlantComponents());
        }

        [TestMethod]
        public void TestListDevices()
        {
            productionPlant.ListDevices();
            persistenceClass.Verify(repository => repository.ListDevices());
        }

        [TestMethod]
        public void TestListInstallations()
        {
            productionPlant.ListInstallations();
            persistenceClass.Verify(repository => repository.ListInstallations());
        }

        [TestMethod]
        public void TestListDeviceTypes()
        {
            productionPlant.ListDeviceTypes();
            persistenceClass.Verify(repository => repository.ListDeviceTypes());
        }

        [TestMethod]
        public void TestNullRepositoryException()
        {
            bool result = false;
            PlantComponent plantComponent = new Installation();
            productionPlant.Repository = null;
            try
            {
                productionPlant.DeletePlantComponent(plantComponent);
                Assert.Fail();
            }
            catch (NullReferenceException)
            {
                result = true;
            }
            Assert.IsTrue(result);
        }
    }
}
