﻿using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DomainTest
{
    [TestClass]
    public class InstallationTest
    {
        private Installation installation;

        [TestInitialize()]
        public void InitializeInstallationTest()
        {
            installation = new Installation();
        }

        [TestMethod]
        public void TestInstallationChildrenComponentsListNotNull()
        {
            Assert.IsNotNull(installation.ChildrenComponents);
        }

        [TestMethod]
        public void TestAddDevice()
        {
            PlantComponent device = CreateDevice();
            installation.AddComponent(device);
            PlantComponent firstComponent = installation.ChildrenComponents[0];
            Assert.AreEqual(device, firstComponent);
        }

        [TestMethod]
        public void TestDeleteComponent()
        {
            PlantComponent device = CreateDevice();
            installation.AddComponent(device);
            installation.DeleteComponent(device);
            Assert.IsTrue(installation.ChildrenComponents.Count == 0);
        }
        [TestMethod]
        public void TestAddInstallation()
        {
            PlantComponent anotherInstallation = CreateAnotherInstallation();
            installation.AddComponent(anotherInstallation);
            PlantComponent firstComponent = installation.ChildrenComponents[0];
            Assert.AreEqual(anotherInstallation, firstComponent);
        }

        [TestMethod]
        public void TestInstallationCountActiveAlarms()
        {
            PlantComponent installationChild = CreateAnotherInstallation();
            installationChild.Name = "Instalación hija";
            installationChild.Parent = installation;
            Variable installationChildVariable = CreateVariableOutOfRange();
            installationChildVariable.Identifier = 1;
            installationChildVariable.Name = "Variable de Instalación hija";
            
            PlantComponent device = CreateDevice();
            device.Parent = installationChild;
            Variable deviceVariable = CreateVariableOutOfRange();
            deviceVariable.Identifier = 2;
            deviceVariable.Name = "Variable de Dispositivo de prueba";
            
            Variable installationVariable = CreateVariableOutOfRange();
            installationVariable.Identifier = 3;
            installationVariable.Name = "Variable de Instalación de prueba";

            device.AddVariable(deviceVariable);
            installationChild.AddVariable(installationChildVariable);
            installation.AddVariable(installationVariable);

            installationChild.AddComponent(device);
            installation.AddComponent(installationChild);

            installation.RefreshOutOfRangeVariableNumber();
            int NumberOfActiveAlarms = installation.OutOfRangeVariablesNumber;
            Assert.AreEqual(NumberOfActiveAlarms, 3);
        }

        [TestMethod]
        public void TestNotifyObservers()
        {
            LoadInstallationData();
            var mockObserver = new Mock<Observer>();
            Observer observer = mockObserver.Object;
            installation.AddObserver(observer);
            installation.NotifyObservers();
            mockObserver.Verify(x => x.Update(It.IsAny<Observable>()));
        }

        [TestMethod]
        public void TestInstallationEquality()
        {
            LoadInstallationData();
            PlantComponent anotherInstallation = CreateAnotherInstallation();
            anotherInstallation.Identifier = installation.Identifier;
            Assert.IsTrue(installation.Equals(anotherInstallation));
        }

        [TestMethod]
        public void TestInstallationToString()
        {
            LoadInstallationData();
            string installationToStringExpected = "Instalación nº: 1010, Nombre: Instalación de prueba";
            Assert.AreEqual(installationToStringExpected, installation.ToString());
        }

        [TestMethod]
        public void TestCopyDataFrom()
        {
            LoadInstallationData();
            Installation newInstallation = new Installation();
            newInstallation.CopyDataFrom(installation);
            Assert.AreEqual(newInstallation.Name, installation.Name);
        }

        [TestMethod]
        public void TestIsDevice()
        {
            PlantComponent newInstallation = new Installation();
            Assert.IsFalse(newInstallation.IsDevice());
        }

        [TestMethod]
        public void TestIsInstallation()
        {
            PlantComponent newInstallation = new Installation();
            Assert.IsTrue(newInstallation.IsInstallation());
        }

        [TestMethod]
        public void TestIsUsed()
        {
            LoadInstallationData();
            Assert.IsFalse(installation.IsUsed());
        }

        [TestMethod]
        public void TestIsEmpty()
        {
            LoadInstallationData();
            Assert.IsFalse(installation.IsEmpty());
        }
        private void LoadInstallationData()
        {
            installation.Identifier = 1010;
            installation.Name = "Instalación de prueba";
        }

        private PlantComponent CreateDevice()
        {
            PlantComponent device = new Device();
            device.Identifier = 1111;
            device.Name = "Dispositivo de prueba";
            device.Parent = installation;
            return device;
        }

        private PlantComponent CreateAnotherInstallation()
        {
            PlantComponent anotherInstallation = new Installation();
            anotherInstallation.Identifier = 2222;
            anotherInstallation.Name = "Otra instalación de prueba";
            anotherInstallation.Parent = installation;
            return anotherInstallation;
        }

        private Variable CreateVariableOutOfRange()
        {
            Variable variable = new Variable();
            variable.MaximumValue = 15.3;
            variable.MinimumValue = -5.2;
            variable.ActualValue = -10.8;
            return variable;
        }


    }
}
