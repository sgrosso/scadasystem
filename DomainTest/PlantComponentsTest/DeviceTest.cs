﻿using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DomainTest
{
    [TestClass]
    public class DeviceTest
    {
        private Device device;
        public DeviceTest() { }

        [TestInitialize()]
        public void DeviceTestInitialize()
        {
            device = new Device();
        }

        [TestMethod]
        public void TestNameNotNull()
        {
            Assert.IsNotNull(device.Name);
        }

        [TestMethod]
        public void TestDeviceVariableListNotNull()
        {
            Assert.IsNotNull(device.Variables);
        }

        [TestMethod]
        public void TestAddVariable()
        {
            Variable expected = CreateAVariable();
            device.AddVariable(expected);
            Variable fistDeviceVariable = device.Variables[0];
            Assert.AreEqual(expected, fistDeviceVariable);
        }
        [TestMethod]

        public void TestModifyVariable()
        {
            Variable aVariable = CreateAVariable();
            device.AddVariable(aVariable);
            Variable anotherVariable = CreateAnotherVariable();
            device.ModifyVariable(anotherVariable);
            Variable firstDeviceVariable = device.Variables[0];
            Assert.AreEqual(anotherVariable.ActualValue, firstDeviceVariable.ActualValue);
        }

        [TestMethod]
        public void TestRemoveVariable()
        {
            Variable aVariable = CreateAVariable();
            device.AddVariable(aVariable);
            Variable removeVariable = CreateAVariable();
            device.DeleteVariable(removeVariable);
            Assert.IsTrue(device.Variables.Count == 0);
        } 

        [TestMethod]
        public void TestCountActiveAlarms()
        {
            Variable aVariable = CreateAVariable();
            aVariable.ActualValue = 18.5;
            device.AddVariable(aVariable);
            int expectedValue = 1;
            device.RefreshOutOfRangeVariableNumber();
            Assert.AreEqual(device.OutOfRangeVariablesNumber,expectedValue);
        }
        [TestMethod]
        public void TestDeviceToString()
        {
            LoadDeviceData();
            string deviceToString = "Dispositivo n°: 2020, Nombre: Dispositivo de prueba";
            Assert.AreEqual(deviceToString, device.ToString());
        }

        [TestMethod]
        public void TestDeviceEquality()
        {
            Device anotherDevice = new Device();
            anotherDevice.Identifier = device.Identifier;
            Assert.AreEqual(device, anotherDevice); 
        }

        [TestMethod]
        public void TestDeviceNotEquality()
        {
            LoadDeviceData();
            Device anotherDevice = new Device();
            Assert.AreNotEqual(device, anotherDevice);
        }

        [TestMethod]
        public void TestCopyDataFrom()
        {
            LoadDeviceData();
            Device anotherDevice = new Device();
            anotherDevice.CopyDataFrom(device);
            Assert.AreEqual(device.Name, anotherDevice.Name);
        }

        [TestMethod]
        public void TestIsDevice()
        {
            PlantComponent device = new Device();
            Assert.IsTrue(device.IsDevice());
        }

        [TestMethod]
        public void TestIsInstallation()
        {
            PlantComponent device = new Device();
            Assert.IsFalse(device.IsInstallation());
        }

        [TestMethod]
        public void TestIsUsedByInstallation()
        {
            PlantComponent newDevice = new Device();
            PlantComponent newInstallation = new Installation();
            newInstallation.AddComponent(newDevice);
            Assert.IsTrue(newDevice.IsUsed());
        }

        [TestMethod]
        public void TestIsNotUsed()
        {
            LoadDeviceData();
            Assert.IsFalse(device.IsUsed());
        }
        private void LoadDeviceData()
        {
            device.Identifier = 2020;
            device.Name = "Dispositivo de prueba";
            device.Type = new DeviceType();
            device.InUse = false;
        }
        private Variable CreateAVariable()
        {
            Variable aVariable = new Variable();
            aVariable.Identifier = 1;
            aVariable.Name = "Variable de prueba";
            aVariable.MinimumValue = -5.8;
            aVariable.MaximumValue = 10.2;
            aVariable.ActualValue = 8.2;
            return aVariable;
        }

        private Variable CreateAnotherVariable()
        {
            Variable anotherVariable = new Variable();
            anotherVariable.Identifier = 1;
            anotherVariable.Name = "Otra variable de prueba";
            anotherVariable.MinimumValue = -5.8;
            anotherVariable.MaximumValue = 10.2;
            anotherVariable.ActualValue = 3.2;
            return anotherVariable;
        }
    }
}
