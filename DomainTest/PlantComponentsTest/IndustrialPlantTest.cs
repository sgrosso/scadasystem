﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Domain;

namespace DomainTest.PlantComponentsTest
{
    [TestClass]
    public class IndustrialPlantTest
    {
        private IndustrialPlant industrialPlant;
   
        [TestInitialize()]
        public void InitializeIndustrialPlantTest()
        {
            industrialPlant = new IndustrialPlant();
        }

        [TestMethod]
        public void TestIndustrialPlantNameNotNull()
        {
            Assert.IsNotNull(industrialPlant.Name);
        }

        [TestMethod]
        public void TestIndustrialAddressNotNull()
        {
            Assert.IsNotNull(industrialPlant.Address);
        }

        [TestMethod]
        public void TestIndustrialPlantCountryNotNull()
        {
            Assert.IsNotNull(industrialPlant.Country);
        }

        [TestMethod]
        public void TestIndustrialPlantCityNotNull()
        {
            Assert.IsNotNull(industrialPlant.City);
        }

        [TestMethod]
        public void TestAddDevice()
        {
            PlantComponent device = CreateDevice();
            industrialPlant.AddComponent(device);
            PlantComponent firstComponent = industrialPlant.ChildrenComponents[0];
            Assert.AreEqual(device, firstComponent);
        }

        public void TestDeleteDevice()
        {
            PlantComponent device = CreateDevice();
            industrialPlant.AddComponent(device);
            industrialPlant.DeleteComponent(device);
            Assert.IsTrue(industrialPlant.ChildrenComponents.Count == 0);
        }

        [TestMethod]
        public void TestAddInstallation()
        {
            PlantComponent installation = CreateInstallation();
            industrialPlant.AddComponent(installation);
            PlantComponent firstComponent = industrialPlant.ChildrenComponents[0];
            Assert.AreEqual(installation, firstComponent);
        }

        [TestMethod]
        public void TestAddIndustrialPlant()
        {
            PlantComponent anotherIndustrialPlant = CreateIndustrialPlant();
            industrialPlant.AddComponent(anotherIndustrialPlant);
            PlantComponent firstComponent = industrialPlant.ChildrenComponents[0];
            Assert.AreEqual(anotherIndustrialPlant, firstComponent);
        }

        [TestMethod]
        public void TestIndustrialPlantCountActiveAlarms()
        {
            PlantComponent installationChild = CreateInstallation();
            installationChild.Name = "Instalación Hija";
            installationChild.Parent = industrialPlant;
            Variable installationChildVariable = CreateVariableOutOfRange();
            installationChildVariable.Identifier = 1;
            installationChildVariable.Name = "Variable de instalación hija";

            PlantComponent device = CreateDevice();
            device.Parent = industrialPlant;
            Variable deviceVariable = CreateVariableOutOfRange();
            deviceVariable.Identifier = 2;
            deviceVariable.Name = "Variable de Dispositivo hijo";

            Variable anotherDeviceVariable = CreateVariableOutOfRange();
            anotherDeviceVariable.Identifier = 3;
            anotherDeviceVariable.Name = "Otra variable del dispositivo";

            device.AddVariable(deviceVariable);
            device.AddVariable(anotherDeviceVariable);
            installationChild.AddVariable(installationChildVariable);
            installationChild.AddComponent(device);
            industrialPlant.AddComponent(installationChild);

            industrialPlant.RefreshOutOfRangeVariableNumber();
            int NumberOfActiveAlarms = industrialPlant.OutOfRangeVariablesNumber;
            Assert.AreEqual(NumberOfActiveAlarms, 3);
        }

        private PlantComponent CreateDevice()
        {
            PlantComponent device = new Device();
            device.Identifier = 1111;
            device.Name = "Dispositivo de prueba";
            device.Parent = industrialPlant;
            return device;
        }

        private PlantComponent CreateInstallation()
        {
            PlantComponent installation = new Installation();
            installation.Identifier = 2222;
            installation.Name = "Instalación de prueba";
            installation.Parent = industrialPlant;
            return installation;
        }

        private PlantComponent CreateIndustrialPlant()
        {
            IndustrialPlant anotherIndustrialPlant = new IndustrialPlant();
            anotherIndustrialPlant.Identifier = 3333;
            anotherIndustrialPlant.Name = "Planta Industrial de Prueba";
            anotherIndustrialPlant.Country = "Uruguay";
            anotherIndustrialPlant.City = "Montevideo";
            anotherIndustrialPlant.Address = "Cuareim 1256";
            PlantComponent industrialPlantReturned = anotherIndustrialPlant;
            return industrialPlantReturned;
        }

        private Variable CreateVariableOutOfRange()
        {
            Variable variable = new Variable();
            variable.MaximumValue = 15.3;
            variable.MinimumValue = -5.2;
            variable.MaximumWarningValue = 10.0;
            variable.MinimumWarningValue = -3.0;
            variable.ActualValue = -10.8;
            return variable;
        }
    }
}
