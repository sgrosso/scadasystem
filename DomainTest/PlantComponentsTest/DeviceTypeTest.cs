﻿using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace DomainTest
{
    [TestClass]
    public class DeviceTypeTest
    {
        private DeviceType deviceType;
        public DeviceTypeTest()
        {
           
        }
        
        private void LoadDeviceTypeData()
        {
            deviceType = new DeviceType();
            deviceType.Id = 10;
            deviceType.Name = "Type";
            deviceType.Description = "A Description";
        }
        [TestMethod]
        public void IdValidTest()
        {
            DeviceType deviceType = new DeviceType();
            Assert.IsNotNull(deviceType.Id);
        }

        [TestMethod]
        public void NameValidTest()
        {
            DeviceType deviceType = new DeviceType();
            Assert.IsNotNull(deviceType.Name);
        }

        [TestMethod]
        public void DescriptionValidTest()
        {
            DeviceType deviceType = new DeviceType();
            Assert.IsNotNull(deviceType.Description);
        }

        [TestMethod]
        public void EqualsTrueTest()
        {
            DeviceType dv1 = new DeviceType();
            dv1.Id = 10;
            DeviceType dv2 = new DeviceType();
            dv2.Id = 10;
            var equals = dv1.Equals(dv2);
            Assert.IsTrue(equals);
        }

        [TestMethod]
        public void EqualsFalseTest()
        {
            DeviceType dv1 = new DeviceType();
            dv1.Id = 10;
            DeviceType dv2 = new DeviceType();
            dv2.Id = 20;
            var equals = dv1.Equals(dv2);
            Assert.IsFalse(equals);
        }

        [TestMethod]
        public void ToStringTest()
        {
            LoadDeviceTypeData();
            String typeToString = deviceType.ToString();
            String toStringExpected = "Type";
            Assert.AreEqual(typeToString, toStringExpected);
        }

        [TestMethod]
        public void CopyDataFromTest()
        {
            LoadDeviceTypeData();
            DeviceType newType = new DeviceType();
            newType.CopyDataFrom(deviceType);
            Assert.AreEqual(newType.Name, deviceType.Name);
        }

        [TestMethod]
        public void TestIsNotEmpty()
        {
            LoadDeviceTypeData();
            Assert.IsFalse(deviceType.IsEmpty());
        }

        [TestMethod]
        public void TestIsEmpty()
        {
            DeviceType newType = new DeviceType();
            Assert.IsTrue(newType.IsEmpty());
        }
    }
}
