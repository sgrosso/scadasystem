﻿using System;
using System.Collections.Generic;

namespace Domain
{
    public class IncidentManager
    {
        public IncidentReporter IncidentReporter { get; set; }
        public IncidentLoader IncidentLoader { get; set; }

        private static IncidentManager instance;

        private IncidentManager() { }

        public static IncidentManager Instance()
        {
            if (instance == null)
            {
                instance = new IncidentManager();
            }
            return instance;
        }

        public void SetReportMethod(IncidentReporter reporter)
        {
            IncidentReporter = reporter;
        }

        public void SetLoadMethod(IncidentLoader loader)
        {
            IncidentLoader = loader;
        }

        public void ReportIncident(Incident incident)
        {
            IncidentReporter.Report(incident);
        }

        public List<Incident> GetIncidentsFilteredBy(Tuple<DateTime,DateTime> dateRangeFilter, int severity, PlantComponent plantComponentFilter, bool executeRecursiveSearch)
        {
            return IncidentLoader.LoadIncidentsFilteringBy(dateRangeFilter, severity, plantComponentFilter, executeRecursiveSearch);
        }

    }
}
