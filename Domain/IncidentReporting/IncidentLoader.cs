﻿using System;
using System.Collections.Generic;

namespace Domain
{
    public interface IncidentLoader
    {
        List<Incident> LoadIncidentsFilteringBy(Tuple<DateTime, DateTime> dateRangeFilter, int severity, PlantComponent plantComponent, bool executeRecursiveSearch);
    }
}
