﻿namespace Domain
{
    public interface IncidentReporter
    {
        void Report(Incident incident);
    }
}
