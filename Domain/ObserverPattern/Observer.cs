﻿namespace Domain
{
    public interface Observer
    {
        void Update(Observable observable);
    }
}
