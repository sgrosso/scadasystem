﻿using System;
using Persistence;
using System.Collections.Generic;

namespace Domain
{
    public class ProductionPlant
    {
        private static ProductionPlant instance;

        public DomainPersistence Repository { get; set; }

        private ProductionPlant()
        {
        }

        public static ProductionPlant GetInstance()
        {
            if (instance == null)
            {
                instance = new ProductionPlant();
            }
            return instance;
        }

        public bool AddPlantComponent(PlantComponent plantComponent)
        {
            try
            { 
                return Repository.AddPlantComponent(plantComponent);
            }
            catch(NullReferenceException)
            {
                throw;
            }
        }

        public bool DeletePlantComponent(PlantComponent plantComponent)
        {
            try
            {
                return Repository.DeletePlantComponent(plantComponent);
            }
            catch(NullReferenceException)
            {
                throw;
            }
        }

        public bool ModifyPlantComponent(PlantComponent plantComponent)
        {
            try
            {
                return Repository.ModifyPlantComponent(plantComponent);
            }
            catch(NullReferenceException)
            {
                throw;
            } 
        }

        public List<PlantComponent> ListPlantComponents()
        {
            try
            {
                return Repository.ListPlantComponents();
            }
            catch (NullReferenceException)
            {
                throw;
            }
        }

        public bool AddInstallation(Installation installation)
        {
            return Repository.AddInstallation(installation);
        }

        public bool DeleteInstallation(Installation installation)
        {
            return Repository.DeleteInstallation(installation);
        }

        public bool ModifyInstallation(Installation installation)
        {
            return Repository.ModifyInstallation(installation);
        }

        public List<Installation> ListInstallations()
        {
            return Repository.ListInstallations();
        }
        public bool AddDevice(Device device)
        {
            return Repository.AddDevice(device);
        }

        public bool DeleteDevice(Device device)
        {
            return Repository.DeleteDevice(device);
        }

        public bool ModifyDevice(Device device)
        {
            return Repository.ModifyDevice(device);
        }

        public List<Device> ListDevices()
        {
            return Repository.ListDevices();
        }

        public bool AddIndustrialPlant(IndustrialPlant industrialPlant)
        {
            return Repository.AddIndustrialPlant(industrialPlant);
        }

        public bool DeleteIndustrialPlant(IndustrialPlant industrialPlant)
        {
            return Repository.DeleteIndustrialPlant(industrialPlant);
        }

        public bool ModifyIndustrialPlant(IndustrialPlant industrialPlant)
        {
            return Repository.ModifyIndustrialPlant(industrialPlant);
        }

        public List<IndustrialPlant> ListIndustrialPlants()
        {
            return Repository.ListIndustrialPlants();
        }

        public bool AddDeviceType(DeviceType deviceType)
        {
            return Repository.AddDeviceType(deviceType);
        }

        public bool ModifyDeviceType(DeviceType deviceType)
        {
            return Repository.ModifyDeviceType(deviceType);
        }

        public List<DeviceType> ListDeviceTypes()
        {
            return Repository.ListDeviceTypes();
        }

        public List<PlantComponent> ListIndustrialPlantChidrenComponents(IndustrialPlant industrialPlant)
        {
            try
            {
                return Repository.ListIndustrialPlantChildrenComponents(industrialPlant);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public List<PlantComponent> ListInstallationChidrenComponents(Installation installation)
        {
            try
            {
                return Repository.ListInstallationChildrenComponents(installation);
            }
            catch(Exception e)
            {
                throw;
            }
        }

        public List<Installation> ListIndustrialPlantFirstLevelInstallations(IndustrialPlant industrialPlant)
        {
            return Repository.ListIndustrialPlantFirstLevelInstallations(industrialPlant);
        }

        public void SetIncidentReportingMethod(IncidentReporter reporter)
        {
            IncidentManager.Instance().SetReportMethod(reporter);
        }

        public void SetIncidentLoadingMethod(IncidentLoader loader)
        {
            IncidentManager.Instance().SetLoadMethod(loader);
        }

        public List<Incident> GetIncidentsFilteredBy(Tuple<DateTime, DateTime> dateRangeFilter, int severity, PlantComponent plantComponentFilter, bool executeRecursiveSearch)
        {
            return IncidentManager.Instance().GetIncidentsFilteredBy(dateRangeFilter, severity, plantComponentFilter, executeRecursiveSearch);
        }

        public PlantComponent GetFirstLevelParent(PlantComponent plantComponent)
        {
            return Repository.GetFirstLevelParent(plantComponent);
        }
    }

}