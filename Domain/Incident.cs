﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Incident : IComparable
    {
        [Key]
        public int Identifier { get; set; }
        public DateTime Date { get; set; }
        public int Severity { get; set; }
        public String Description { get; set; }
        public int PlantComponentIdentifier { get; set; }
        public String PlantComponentName { get; set; }

        public override string ToString()
        {
            return "Date: " + Date.ToShortDateString() + ", " +
                   "Severity: " + Severity.ToString() + ", " +
                   "Description: " + Description.ToString() + ", " + 
                   "Component Name: " + PlantComponentName.ToString();
        }

        public int CompareTo(object obj)
        {
            return Date.CompareTo(((Incident)obj).Date);
        }
    }
}
