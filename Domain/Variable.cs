﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain
{
    public class Variable
    {
        private double actualValue;
        public Variable()
        {
            Identifier = 0;
            actualValue = 0;
            MinimumWarningValue = double.MinValue;
            MaximumWarningValue = double.MaxValue;
            MinimumValue = double.MinValue;
            MaximumValue = double.MaxValue;
            Name = "New Variable";
        }

        [Key]
        [Column("Id")]
        public int Identifier { get; set; }
        public double ActualValue
        {
            get
            {
                return actualValue;
            }
            set
            {
                if (HistoricalValues == null)
                {
                    HistoricalValues = new List<Tuple<double, DateTime>>();
                }
                else
                {
                    AddActualValueToHistoricalWithTodayDate();
                }
                actualValue = value;
            }
        }
        public double MinimumValue { get; set; }
        public String Name { get; set; }
        public double MaximumValue { get; set; }
        public List<Tuple<double, DateTime>> HistoricalValues { get; set; }
        public double MaximumWarningValue { get; set; }
        public double MinimumWarningValue { get; set; }

        private void AddActualValueToHistoricalWithTodayDate()
        {
            Tuple<double, DateTime> historicalItem = new Tuple<double, DateTime>(this.ActualValue, DateTime.Now);
            HistoricalValues.Add(historicalItem);
        }

        public void CopyDataFrom(Variable aVariable)
        {
            if(aVariable != null)
            {
                Name = aVariable.Name;
                MinimumValue = aVariable.MinimumValue;
                MaximumValue = aVariable.MaximumValue;
                MinimumWarningValue = aVariable.MinimumWarningValue;
                MaximumWarningValue = aVariable.MaximumWarningValue;
                ActualValue = aVariable.ActualValue;
            }
        }

        public bool IsVariableValueInRange()
        {
            return (ActualValue >= MinimumValue) && (ActualValue <= MaximumValue);
        }

        public bool isVariableValueInWarningRange()
        {
            return (ActualValue >= MinimumWarningValue) && (ActualValue <= MaximumWarningValue);
        }

        public override int GetHashCode()
        {
            return Identifier.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (Convert.IsDBNull(obj))
                return false;

            Variable variable = obj as Variable;
            return Identifier.Equals(variable.Identifier);
        }

        public override string ToString()
        {
             StringBuilder stringBuilder = new StringBuilder();
             stringBuilder.Append(Name);
             stringBuilder.Append(", actual value: ");
             stringBuilder.Append(ActualValue.ToString());
             return stringBuilder.ToString();
        }
    }

}