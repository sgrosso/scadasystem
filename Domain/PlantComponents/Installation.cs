﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain
{
    [Table("Installations")]
    public class Installation : PlantComponent
    {
        public virtual List<PlantComponent> ChildrenComponents { get; set; }

        public Installation()
        {
            ChildrenComponents = new List<PlantComponent>();
        }

        public override void AddComponent(PlantComponent plantComponent)
        {
            if (plantComponent.IsProductionPlant())
            {
                throw new Exception("Este componente no puede agregarse a una instalación.");
            }
            else
            {
                if (!ExistsPlantComponent(plantComponent))
                {
                    plantComponent.Parent = this;
                    ChildrenComponents.Add(plantComponent);
                    SendOperatingData();
                }
            }
        }

        public override void DeleteComponent(PlantComponent plantComponent)
        {
            if (ExistsPlantComponent(plantComponent))
            {
                PlantComponent childInCollection = GetChildComponent(plantComponent);
                childInCollection.Parent = null;
                ChildrenComponents.Remove(plantComponent);
                SendOperatingData();
            }
        }

        private bool ExistsPlantComponent(PlantComponent plantComponent)
        {
            return ChildrenComponents.Contains(plantComponent);
        }

        private PlantComponent GetChildComponent(PlantComponent child)
        {
            int NOT_FOUND_INDEX = -1;
            PlantComponent foundChild = null;
            int foundChildComponentIndex = ChildrenComponents.IndexOf(child);
            if (foundChildComponentIndex != NOT_FOUND_INDEX)
            {
                foundChild = ChildrenComponents[foundChildComponentIndex];
            }
            return foundChild;
        }

        public override void RefreshOutOfRangeVariableNumber()
        {
            int numberOfActiveAlarms = CountInstallationVariablesOutOfRange();
            int numberOfActiveWarnings = CountInstallationVariablesOutOfWarningRange();
            foreach (PlantComponent plantComponent in ChildrenComponents)
            {
                plantComponent.RefreshOutOfRangeVariableNumber();
                numberOfActiveAlarms += plantComponent.OutOfRangeVariablesNumber;
                numberOfActiveWarnings += plantComponent.OutOfWarningRangeVariablesNumber;
            }
            OutOfRangeVariablesNumber = numberOfActiveAlarms;
            OutOfWarningRangeVariablesNumber = numberOfActiveWarnings;
        }

        private int CountInstallationVariablesOutOfRange()
        {
            int activeAlarmsNumber = 0;
            foreach (Variable aVariable in Variables)
            {
                if (!aVariable.IsVariableValueInRange())
                {
                    activeAlarmsNumber++;
                }
            }
            return activeAlarmsNumber;
        }

        private int CountInstallationVariablesOutOfWarningRange()
        {
            int activeWarningsNumber = 0;
            foreach(Variable aVariable in Variables)
            {
                if(!aVariable.isVariableValueInWarningRange())
                {
                    activeWarningsNumber++;
                }
            }
            return activeWarningsNumber;
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("Installation nº: ");
            stringBuilder.Append(Identifier);
            stringBuilder.Append(", Name: ");
            stringBuilder.Append(Name);
            return stringBuilder.ToString();
        }

        public void CopyDataFrom(Installation anInstallation)
        {
            if (anInstallation != null)
            {
                Name = anInstallation.Name;
                Variables = anInstallation.Variables;
                CopyComponentsFrom(anInstallation);
            }
        }

        private void CopyComponentsFrom(Installation anInstallation)
        {
            foreach(PlantComponent aComponent in anInstallation.ChildrenComponents)
            {
                aComponent.Parent = this;
            }
            ChildrenComponents = anInstallation.ChildrenComponents;
        }

        public override bool IsInstallation()
        {
            return true;
        }

        public override bool IsDevice()
        {
            return false;
        }

        public override bool IsProductionPlant()
        {
            return false;
        }

        public override void SetInUseBy(PlantComponent plantComponent)
        {
            Parent = plantComponent;
        }

        public override void SetNotInUse()
        {
            Parent = null;
        }
    }
}