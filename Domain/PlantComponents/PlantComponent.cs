﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    public abstract class PlantComponent : Observable
    {
        public PlantComponent()
        {
            Identifier = 0;
            Name = "New Component";
            Variables = new List<Variable>();
            Parent = null;
        }
        [Key]
        public int Identifier { get; set; }
        public String Name { get; set; }
        public virtual List<Variable> Variables { get; set; }
        public virtual PlantComponent Parent { get; set; }
        public int OutOfRangeVariablesNumber { get; set; }
        public int OutOfWarningRangeVariablesNumber { get; set;}

        public abstract void RefreshOutOfRangeVariableNumber();

        public virtual void AddVariable(Variable aVariable)
        {
            if (!ExistsVariable(aVariable))
            {
                Variables.Add(aVariable);
                ProductionPlant.GetInstance().ModifyPlantComponent(this);
                SendOperatingData();
            }
        }

        public virtual void ModifyVariable(Variable aVariable)
        {
            if (ExistsVariable(aVariable))
            {
                Variable variableInCollection = GetVariable(aVariable);
                variableInCollection.CopyDataFrom(aVariable);
                ProductionPlant.GetInstance().ModifyPlantComponent(this);
                SendOperatingData();
            }
        }

        public virtual void DeleteVariable(Variable aVariable)
        {
            if (ExistsVariable(aVariable))
            {
                Variables.Remove(aVariable);
                ProductionPlant.GetInstance().ModifyPlantComponent(this);
                SendOperatingData();
            }
        }

        private Variable GetVariable(Variable searchVariable)
        {
            int NOT_FOUND_INDEX = -1;
            Variable foundVariable = null;
            int variableIndex = Variables.IndexOf(searchVariable);
            if (variableIndex != NOT_FOUND_INDEX)
            {
                foundVariable = Variables[variableIndex];
            }
            return foundVariable;
        }

        private bool ExistsVariable(Variable aVariable)
        {
            return Variables.Contains(aVariable);
        }
        
        public PlantComponent GetFirstLevelParent()
        {
            return ProductionPlant.GetInstance().GetFirstLevelParent(this);
        }

        public virtual void AddComponent(PlantComponent plantComponent)
        {
            return;
        }

        public virtual void DeleteComponent(PlantComponent plantComponent)
        {
            return;
        }

        public void SendOperatingData()
        {
            PlantComponent firstLevelParent = GetFirstLevelParent();
            firstLevelParent.RefreshOutOfRangeVariableNumber();
            firstLevelParent.NotifyObservers();
        }

        public override int GetHashCode()
        {
            return Identifier.GetHashCode();
        }
        public abstract bool IsDevice();

        public abstract bool IsInstallation();

        public abstract bool IsProductionPlant();

        public bool IsEmpty()
        {
            return Identifier == 0;
        }

        public virtual bool IsUsed()
        {
            return Parent != null;
        }

        public abstract void SetInUseBy(PlantComponent plantComponent);

        public abstract void SetNotInUse();

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (Convert.IsDBNull(obj))
                return false;

            PlantComponent plantComponent = obj as PlantComponent;
            return Identifier.Equals(plantComponent.Identifier);
        }

        public void ReportIncident(Incident incident)
        {
            incident.PlantComponentIdentifier = Identifier;
            incident.PlantComponentName = Name;
            IncidentManager.Instance().ReportIncident(incident);
        }
    }
}
