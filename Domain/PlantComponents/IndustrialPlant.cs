﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    [Table("IndustrialPlants")]
    public class IndustrialPlant : PlantComponent
    {
        public IndustrialPlant()
        {
            Country = String.Empty;
            City = String.Empty;
            Address = String.Empty;
            ChildrenComponents = new List<PlantComponent>();
        }

        public string Address { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public List<PlantComponent> ChildrenComponents { get; set; }

        public override bool IsDevice()
        {
            return false;
        }

        public override bool IsInstallation()
        {
            return false;
        }
        public override bool IsProductionPlant()
        {
            return true;
        }

        public override void RefreshOutOfRangeVariableNumber()
        {
            int numberOfActiveAlarms = 0;
            int numberOfActiveWarnings = 0;

            foreach (var component in ChildrenComponents)
            {
                component.RefreshOutOfRangeVariableNumber();
                numberOfActiveAlarms += component.OutOfRangeVariablesNumber;
                numberOfActiveWarnings += component.OutOfWarningRangeVariablesNumber;
            }

            OutOfRangeVariablesNumber = numberOfActiveAlarms;
            OutOfWarningRangeVariablesNumber = numberOfActiveWarnings;
        }

        public override void SetInUseBy(PlantComponent plantComponent)
        {
            Parent = plantComponent;
        }

        public override void SetNotInUse()
        {
            Parent = null;
        }

        public override void AddComponent(PlantComponent plantComponent)
        {
            if (!ExistsPlantComponent(plantComponent))
            {
                plantComponent.Parent = this;
                ChildrenComponents.Add(plantComponent);
                SendOperatingData();
            }
        }

        public override void DeleteComponent(PlantComponent plantComponent)
        {
            if (ExistsPlantComponent(plantComponent))
            {
                PlantComponent childInCollection = GetChildComponent(plantComponent);
                childInCollection.Parent = null;
                ChildrenComponents.Remove(plantComponent);
                SendOperatingData();
            }
        }

        private PlantComponent GetChildComponent(PlantComponent child)
        {
            int NOT_FOUND_INDEX = -1;
            PlantComponent foundChild = null;
            int foundChildComponentIndex = ChildrenComponents.IndexOf(child);
            if (foundChildComponentIndex != NOT_FOUND_INDEX)
            {
                foundChild = ChildrenComponents[foundChildComponentIndex];
            }
            return foundChild;
        }

        private bool ExistsPlantComponent(PlantComponent plantComponent)
        {
            return ChildrenComponents.Contains(plantComponent);
        }

        public override bool IsUsed()
        {
            return true;
        }

        public override void AddVariable(Variable aVariable)
        {
            throw CreateVariableException();
            
        }

        public override void DeleteVariable(Variable aVariable)
        {
            throw CreateVariableException();
        }

        public override void ModifyVariable(Variable aVariable)
        {
            throw CreateVariableException();
        }

        private Exception CreateVariableException()
        {
            return new Exception("Este componente no acepta el ingreso de variables.");
        }

        public void CopyDataFrom(IndustrialPlant anIndustrialPlant)
        {
            if (anIndustrialPlant != null)
            {
                Country = anIndustrialPlant.Country;
                City = anIndustrialPlant.City;
                Address = anIndustrialPlant.Address;
                CopyComponentsFrom(anIndustrialPlant);
            }
        }

        private void CopyComponentsFrom(IndustrialPlant anIndustrialPlant)
        {
            foreach (PlantComponent aComponent in anIndustrialPlant.ChildrenComponents)
            {
                aComponent.Parent = this;
            }
            ChildrenComponents = anIndustrialPlant.ChildrenComponents;
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("Industrial Plant nº: ");
            stringBuilder.Append(Identifier);
            stringBuilder.Append(", Name: ");
            stringBuilder.Append(Name);
            return stringBuilder.ToString();
        }
    }
}
