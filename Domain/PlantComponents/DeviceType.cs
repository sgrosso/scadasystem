﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    public class DeviceType
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }

        private string name;
        public string Name {
            get{
                return (String.IsNullOrWhiteSpace(name)) ? "" : name;
            }
            set{
                name = value;
            }
        }

        private string description;
        public string Description {
            get {
                return (String.IsNullOrWhiteSpace(description)) ? "" : description;
            }
            set {
                description = value;
            }
        }

        public DeviceType()
        {
            Id = 0;
            Name = "Name";
            Description = "Description";
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override bool Equals(Object obj)
        {
            if (obj == null)
            {
                return false;
            }
            if (Convert.IsDBNull(obj))
            {
                return false;
            }
            DeviceType device = obj as DeviceType;
            return Id.Equals(device.Id);
        }

        public override string ToString()
        {
            return Name;
        }

        public bool IsEmpty()
        {
            return Id == 0;
        }

        public void CopyDataFrom(DeviceType deviceType)
        {
            Name = deviceType.Name;
            Description = deviceType.Description;
        }
    }
}