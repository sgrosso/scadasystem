﻿using System;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    [Table("Devices")]
    public class Device : PlantComponent
    {
        public virtual DeviceType Type { get; set; }
        public bool InUse { get; set; }

        public override void RefreshOutOfRangeVariableNumber()
        {
            int numberOfActiveAlarms = 0;
            int numberOfActiveWarnings = 0;
            foreach (Variable aVariable in Variables)
            {
                if (!aVariable.IsVariableValueInRange())
                {
                    numberOfActiveAlarms++;
                }

                if (!aVariable.isVariableValueInWarningRange())
                {
                    numberOfActiveWarnings++;
                }
            }
            OutOfRangeVariablesNumber = numberOfActiveAlarms;
            OutOfWarningRangeVariablesNumber = numberOfActiveWarnings;
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("Device n°: ");
            stringBuilder.Append(Identifier.ToString());
            stringBuilder.Append(", Name: ");
            stringBuilder.Append(Name.ToString());
            return stringBuilder.ToString();
        }

        public void CopyDataFrom(Device aDevice)
        {
            if (aDevice != null)
            {
                Name = aDevice.Name;
                Variables = aDevice.Variables;
                Type = aDevice.Type;
                InUse = aDevice.InUse;
            }
        }

        public override bool IsDevice()
        {
            return true;
        }

        public override bool IsInstallation()
        {
            return false;
        }

        public override bool IsProductionPlant()
        {
            return false;
        }

        public override bool IsUsed()
        {
            return Parent != null || InUse;
        }

        public override void SetInUseBy(PlantComponent plantComponent)
        {
            Parent = plantComponent;
            InUse = true;
        }

        public override void SetNotInUse()
        {
            InUse = false;
        }
    }
}