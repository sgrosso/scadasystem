﻿using Domain;
using System.Collections.Generic;

namespace Persistence
{
    public interface DomainPersistence
    {
        bool AddPlantComponent(PlantComponent plantComponent);
        bool ModifyPlantComponent(PlantComponent plantComponent);
        bool DeletePlantComponent(PlantComponent plantComponent);
        List<PlantComponent> ListPlantComponents();

        bool AddDevice(Device device);
        bool ModifyDevice(Device device);
        bool DeleteDevice(Device device);
        List<Device> ListDevices();

        bool AddInstallation(Installation installation);
        bool ModifyInstallation(Installation installation);
        bool DeleteInstallation(Installation installation);
        List<Installation> ListInstallations();
        List<PlantComponent> ListInstallationChildrenComponents(Installation installation);

        List<PlantComponent> ListIndustrialPlantChildrenComponents(IndustrialPlant industrialPlant);

        PlantComponent GetFirstLevelParent(PlantComponent plantComponent);

        bool AddIndustrialPlant(IndustrialPlant industrialPlant);
        bool ModifyIndustrialPlant(IndustrialPlant industrialPlant);
        bool DeleteIndustrialPlant(IndustrialPlant industrialPlant);
        List<IndustrialPlant> ListIndustrialPlants();

        bool AddDeviceType(DeviceType deviceType);
        bool ModifyDeviceType(DeviceType deviceType);
        List<DeviceType> ListDeviceTypes();
        List<Installation> ListIndustrialPlantFirstLevelInstallations(IndustrialPlant industrialPlant);
    }
}
