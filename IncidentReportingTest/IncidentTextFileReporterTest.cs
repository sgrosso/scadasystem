﻿using DataBasePersistence;
using Domain;
using IncidentReporting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace IncidentReportingTest
{
    [TestClass]
    public class IncidentTextFileReporterTest
    {
        private IncidentTextFileReporter reporter;

        [TestInitialize()]
        public void InitializeIncidentTextFileReporterTest()
        {
            reporter = new IncidentTextFileReporter();
        }
        [TestMethod]
        public void LoadIncidentsFilteringByTest()
        {
            PlantComponent pl = new Installation();
            ProductionPlant.GetInstance().Repository = new DataBaseQueries();
            ProductionPlant.GetInstance().AddPlantComponent(pl);
            Incident incident = new Incident();
            incident.PlantComponentIdentifier = pl.Identifier;
            incident.PlantComponentName = pl.Name;
            incident.Severity = 5;
            incident.Date = DateTime.Today;
            reporter.Report(incident);
            Tuple<DateTime, DateTime> dateFilterRange = new Tuple<DateTime, DateTime>(DateTime.Today, DateTime.Today);
            IncidentTextFileLoader loader = new IncidentTextFileLoader();
        //    List<Incident> incidentReport = loader.LoadIncidentsFilteringBy(dateFilterRange, pl, false);
           // Assert.AreEqual(incidentReport.Count, 1);
        }
    }
}
