﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using IncidentReporting;
using Domain;
using System;
using System.Collections.Generic;
using DataBasePersistence;

namespace IncidentReportingTest
{

    [TestClass]
    public class IncidentDatabaseLoaderTest
    {
        private IncidentDatabaseLoader incidentDataBaseLoader;

        [TestInitialize()]
        public void InitializeIncidentDatabaseLoaderTest()
        {
            incidentDataBaseLoader = new IncidentDatabaseLoader();
        }
        [TestMethod]
        public void LoadIncidentsFilteringByTest()
        {
            PlantComponent pl = new Installation();
            ProductionPlant.GetInstance().Repository = new DataBaseQueries();
            ProductionPlant.GetInstance().AddPlantComponent(pl);
            IncidentDatabaseReporter reporter = new IncidentDatabaseReporter();
            Incident incident = new Incident();
            incident.PlantComponentIdentifier = pl.Identifier;
            incident.PlantComponentName = pl.Name;
            incident.Severity = 5;
            incident.Date = DateTime.Today;
            reporter.Report(incident);
            Tuple<DateTime, DateTime> dateFilterRange = new Tuple<DateTime, DateTime>(DateTime.Today, DateTime.Today);
           // List<Incident> incidentReport = incidentDataBaseLoader.LoadIncidentsFilteringBy(dateFilterRange, pl, false);
           // Assert.AreEqual(incidentReport.Count, 1);
        }
    }
}
